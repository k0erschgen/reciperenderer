package de.rk.tools.health.cookbook.external.fddb;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;


public class FddbDataTest {

    private FddbData fddbData;

    @Before
    public void setUp() throws Exception {
        fddbData = new FddbData("Ingredient", 100.0f, 1l);
    }

    @Test
    public void testSimpleAmountCalculation() {
        FddbNutritionData fddbNutritionData = new FddbNutritionData("Data", "10.0", "g");
        assertThat(fddbData.getValueForRealAmount(fddbNutritionData, 20.0f)).isEqualTo(2.0f);
        assertThat(fddbData.getValueForRealAmount(fddbNutritionData, 35.0f)).isEqualTo(3.5f);
    }

    @Test
    public void testFormattedAmountCalculation() {
        fddbData.addData(new FddbNutritionData("One", "37,3", "g"));
        assertThat(fddbData.getFormattedValueForRealAmount("One", 75.0f)).isEqualTo("28 g");
        assertThat(fddbData.getFormattedValueForRealAmount("One", 150.0f)).isEqualTo("56 g");
    }

    @Test
    public void setEntriesReInitializesInternalCollections() {
        fddbData.addData(new FddbNutritionData("One", "2,5", "g"));
        assertThat(fddbData.getEntries().size()).isEqualTo(1);
        assertThat(fddbData.getFddbDataByIngredientName("One")).isNotNull();

        fddbData.addData(new FddbNutritionData("Two", "5,2", "g"));
        assertThat(fddbData.getEntries().size()).isEqualTo(2);
        assertThat(fddbData.getFddbDataByIngredientName("Two")).isNotNull();

        fddbData.setEntries(Arrays.asList(
                new FddbNutritionData("ReplacedOne", "225", "kcal")
        ));
        assertThat(fddbData.getEntries().size()).isEqualTo(1);
        assertThat(fddbData.getFddbDataByIngredientName("One")).isNull();
        assertThat(fddbData.getFddbDataByIngredientName("Two")).isNull();
        assertThat(fddbData.getFddbDataByIngredientName("ReplacedOne")).isNotNull();
    }

}
package de.rk.tools.health.cookbook;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TestUtils {

    private static final Charset CHARSET = StandardCharsets.UTF_8;

    public static final String BASE_FOLDER = "data/test";

    public static final String VALIDATION_FOLDER = BASE_FOLDER + "/validation";

    public static final String OUTPUT_FOLDER = BASE_FOLDER + "/output";

    public static void validate(String testName, File outfile) throws Exception {
        String content = FileUtils.readFileToString(outfile, CHARSET);
        validate(testName, content);
    }

    public static void validate(String testName, String content) throws Exception {
        org.junit.Assert.assertNotNull(content);
        createFolder(OUTPUT_FOLDER);
        String normalized = normalizeAndMask(content);
        writeResultToFile(testName, normalized, false);
        if (!validationFileExists(testName)) {
            writeNewValidationFile(testName, normalized);
        }
        String validation = readValidationFile(testName);
        org.junit.Assert.assertEquals(validation, normalized);
    }

    private static String normalizeAndMask(String content) {
        return content
                .replaceAll("\r\n", "\n")
                .replaceAll("\\s*\n", "\n")
                .replaceAll(
                        "\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}",
                        "[A-DATE]"
                );
    }

    private static void writeResultToFile(String testName, String content, boolean toValidationFolder) throws IOException {
        Path path;
        if (toValidationFolder) {
            path = Paths.get(VALIDATION_FOLDER, testName + ".txt").toAbsolutePath();
        } else {
            path = Paths.get(OUTPUT_FOLDER, testName + ".txt").toAbsolutePath();
        }
        Files.write(path, content.getBytes(CHARSET));
    }

    private static void writeNewValidationFile(String testName, String content) throws IOException {
        writeResultToFile(testName, "===== new validation file for " + testName + " =====\n" + content, true);
    }

    private static String readValidationFile(String testName) throws IOException {
        return FileUtils.readFileToString(new File(VALIDATION_FOLDER + "/" + testName + ".txt"), CHARSET);
    }

    private static boolean validationFileExists(String testName) {
        return new File(VALIDATION_FOLDER + "/" + testName + ".txt").exists();
    }

    public static void createFolder(String target) throws Exception {
        File targetFolder = Paths.get(target).toAbsolutePath().toFile();
        if (!targetFolder.exists()) {
            Files.createDirectory(Paths.get(target).toAbsolutePath());
        }
    }

}

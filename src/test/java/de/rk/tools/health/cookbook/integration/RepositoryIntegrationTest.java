package de.rk.tools.health.cookbook.integration;

import de.rk.tools.health.cookbook.data.CategoryAndUnit;
import de.rk.tools.health.cookbook.data.Nutrition;
import de.rk.tools.health.cookbook.data.NutritionDataHandler;
import de.rk.tools.health.cookbook.wrapper.NutritionWrapper;
import de.rk.tools.health.cookbook.wrapper.RecipeWrapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringJUnit4ClassRunner.class)
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//@TestPropertySource(locations = "classpath:application.properties")
public class RepositoryIntegrationTest extends AbstractPersistenceIntegrationTest {

    @Before
    public void setup() throws Exception {
        setupRecipeData();
        setupNutritionData();
    }

    //@Test @Rollback(false) public void test() {  }

    @Test
    public void testTypicalRecipeLoad() throws Exception {
        RecipeWrapper recipe = new RecipeWrapper(recipeRepository.findBySrcId("0008"));
        recipe.setNutritionDataHandler(
                new NutritionDataHandler(
                        nutritionRepository.findBySrcIdIn(recipe.getRecipeIngredientNutritionInfoIds())
                                .stream().map(item -> new NutritionWrapper(item)).collect(Collectors.toList())));
        assertThat(recipe.getNutritionValueFormatted("Kalorien")).isEqualTo("Kalorien: 425,51");
        assertThat(recipe.getRecipeIngredients().get(0).getAmount()).isEqualTo(300.0f);

        recipe.setFactor(0.75f);
        assertThat(recipe.getNutritionValueFormatted("Kalorien")).isEqualTo("Kalorien: 425,51");
        assertThat(recipe.getRecipeIngredients().get(0).getAmount()).isEqualTo(225.0f);

        recipe.setFactor(null);
        assertThat(recipe.getNutritionValueFormatted("Kalorien")).isEqualTo("Kalorien: 425,51");
        assertThat(recipe.getRecipeIngredients().get(0).getAmount()).isEqualTo(300.0f);
    }

    @Test
    public void testSimpleCrudOperations() throws Exception {
        RecipeWrapper recipe0008 = new RecipeWrapper(recipeRepository.findBySrcId("0008"));
        assertThat(recipe0008.getName()).isEqualTo("Veganes Naan Brot aus der Pfanne");
        recipe0008.setName("Something else");
        recipeRepository.save(recipe0008.getRecipe());
        assertThat(recipeRepository.findBySrcId("0008").getName()).isEqualTo("Something else");
        Long recipeDbId = recipe0008.getId();
        recipeRepository.deleteById(recipeDbId);
        assertThat(recipeRepository.findBySrcId("0008")).isNull();

        recipe0008.setNutritionDataHandler(
                new NutritionDataHandler(
                        nutritionRepository.findBySrcIdIn(recipe0008.getRecipeIngredientNutritionInfoIds())
                                .stream().map(item -> new NutritionWrapper(item)).collect(Collectors.toList())));

        Nutrition nutrition409 = nutritionRepository.findBySrcId(409);
        assertThat(nutrition409.getIngredientName()).isEqualTo("Aubergine");
        nutrition409.setIngredientName("Aubergine-gekocht");
        nutritionRepository.save(nutrition409);
        assertThat(nutritionRepository.findBySrcId(409).getIngredientName()).isEqualTo("Aubergine-gekocht");
        Long nutritionDbId = nutrition409.getId();
        nutritionRepository.deleteById(nutritionDbId);
        assertThat(nutritionRepository.findBySrcId(409)).isNull();

        List<CategoryAndUnit> distinctCategoryAndUnits = nutritionDataRepository.findDistinctCategoryAndUnits();
        assertThat(distinctCategoryAndUnits.size()).isEqualTo(30);
    }

}

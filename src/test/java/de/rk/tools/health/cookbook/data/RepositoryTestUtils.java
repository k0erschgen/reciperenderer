package de.rk.tools.health.cookbook.data;

import de.rk.tools.health.cookbook.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class RepositoryTestUtils {

    public static Nutrition addNutrition(Long srcId, String name) {
        Nutrition nutrition = new Nutrition(srcId, name, 100f, "g");
        return nutrition;
    }

    public static List<NutritionData> addNutritionData(Nutrition nutrition, Float first, Float second, Float third) {
        List<NutritionData> nutritionDataList = new ArrayList<>();
        nutritionDataList.add(new NutritionData("Kalorien", first, "kcal", nutrition));
        nutritionDataList.add(new NutritionData("Kohlenhydrate", second, "g", nutrition));
        nutritionDataList.add(new NutritionData("Protein", third, "g", nutrition));
        return nutritionDataList;
    }

    public static Recipe getSimpleRecipe(String srcId, String name, float servings) {
        Recipe recipe = new Recipe();
        recipe.setSrcId(srcId);
        recipe.setName(name);
        recipe.setServings(servings);
        recipe.setImgUrl("http://some.url.com");
        recipe.setInstructions("Instruction text");
        return recipe;
    }

    public static List<RecipeIngredient> getSimpleRecipeIngredients(List<String> ingredientStrings) {
        List<RecipeIngredient> ingredients = ingredientStrings.stream().map(s -> Utils.parseIngredient(s).getRecipeIngredient()).collect(Collectors.toList());
        AtomicLong pos = new AtomicLong(1);
        ingredients.forEach(recipeIngredient -> recipeIngredient.setPosNr(pos.getAndIncrement()));
        return ingredients;
    }
}

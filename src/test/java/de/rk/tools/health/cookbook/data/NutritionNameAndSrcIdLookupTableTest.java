package de.rk.tools.health.cookbook.data;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NutritionNameAndSrcIdLookupTableTest {

    @Mock
    private NutritionRepository nutritionRepository;

    private NutritionNameAndSrcIdLookupTable lookupTable;

    @Before
    public void setup() throws Exception {
        when(nutritionRepository.findByIngredientNameIsNotNullOrderByIngredientName()).thenReturn(Arrays.asList(
                new TestIngredientAndName("ABC", 673L),
                new TestIngredientAndName("MNO", 219L),
                new TestIngredientAndName("RST", 1055L),
                new TestIngredientAndName("XYZ", 74L)
        ));
        lookupTable = NutritionNameAndSrcIdLookupTable.getFromNutritionRepository(nutritionRepository);
    }

    @Test
    public void testConstructorUsesRepository() throws Exception {
        verify(nutritionRepository).findByIngredientNameIsNotNullOrderByIngredientName();
    }

    @Test
    public void testBiDirectionalQueries() throws Exception {
        // assertJ throws a strange exception here. Use junit instead
        org.junit.Assert.assertEquals(lookupTable.getIngredientNameBySrcId(219L), "MNO");
        org.junit.Assert.assertEquals(lookupTable.getSrcIdByIngredientName("RST"), Long.valueOf(1055));
    }

    @Test
    public void testNamesAndIdsMap() {
        final String stringified = lookupTable.getNamesAndIdsMap()
                .keySet().stream().sorted()
                .map(k -> k + "/" + lookupTable.getNamesAndIdsMap().get(k))
                .collect(Collectors.joining(","));
        // assertJ throws a strange exception here. Use junit instead
        org.junit.Assert.assertEquals(stringified, "ABC/673,MNO/219,RST/1055,XYZ/74");
    }

    class TestIngredientAndName implements IngredientNameAndSrcId {
        private String ingredientName;
        private Long srcId;

        public TestIngredientAndName(String ingredientName, Long srcId) {
            this.ingredientName = ingredientName;
            this.srcId = srcId;
        }

        @Override
        public String getIngredientName() {
            return ingredientName;
        }

        @Override
        public Long getSrcId() {
            return srcId;
        }
    }

}
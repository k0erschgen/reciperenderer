package de.rk.tools.health.cookbook.external.fddb;

import de.rk.tools.health.cookbook.Application;
import de.rk.tools.health.cookbook.ApplicationConfiguration;
import de.rk.tools.health.cookbook.wrapper.NutritionWrapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(value = "file:${user.home}/cookbook.properties")
public class FddbNutritionDataReaderTest {

    @Autowired
    private ApplicationConfiguration configuration;

    @Test
    public void testReferenceUnitRetrieval() throws Exception {
        FddbData fddbData = new FddbNutritionDataReader(configuration).readData(1066l);
        assertThat(fddbData.getReferenceUnit()).isEqualTo("g");
    }

    @Test
    public void testReadNutritionData() throws Exception {
        List<NutritionWrapper> nutritionWrappers = new FddbNutritionDataReader(configuration).readNutritionData(Arrays.asList(8L, 409L));
        assertThat(nutritionWrappers.size()).isEqualTo(2);
        assertThat(nutritionWrappers.get(0).getSrcId()).isEqualTo(8L);
        assertThat(nutritionWrappers.get(1).getSrcId()).isEqualTo(409L);
        assertThat(nutritionWrappers.get(0).getIngredientName()).contains("Zwiebel");
        assertThat(nutritionWrappers.get(1).getIngredientName()).contains("Aubergine");
    }

}

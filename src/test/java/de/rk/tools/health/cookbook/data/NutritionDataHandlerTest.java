package de.rk.tools.health.cookbook.data;

import de.rk.tools.health.cookbook.wrapper.NutritionWrapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class NutritionDataHandlerTest {

    private List<NutritionWrapper> nutritionWrappers;

    @Before
    public void setup() throws Exception {
        Nutrition nutritionOne = new Nutrition(201l, "abc", 100.0f, "g");
        nutritionOne.setNutritionData(Arrays.asList(
                new NutritionData("Protein", 13.6f, "g", nutritionOne),
                new NutritionData("Kalzium", 255.0f, "mg", nutritionOne)
        ));
        Nutrition nutritionTwo = new Nutrition(201l, "xyz", 100.0f, "g");
        nutritionTwo.setNutritionData(Arrays.asList(
                new NutritionData("Protein", 8.1f, "g", nutritionTwo),
                new NutritionData("Kalzium", 77.9f, "mg", nutritionTwo)
        ));
        nutritionWrappers = Arrays.asList(
                new NutritionWrapper(nutritionOne),
                new NutritionWrapper(nutritionTwo)
        );
    }

    @Test
    public void testGetValueForReturnsZeroForEmptyNutritionList() throws Exception {
        NutritionDataHandler nutritionDataHandler = new NutritionDataHandler(Collections.emptyList());
        assertThat(nutritionDataHandler.getValueFor("Protein", 23l, 300.0f)).isEqualTo(0.0f);
    }

    @Test
    public void test() throws Exception {
        NutritionDataHandler nutritionDataHandler = new NutritionDataHandler(nutritionWrappers);
        assertThat(nutritionDataHandler.getValueFor("Protein", 201l, 200.0f)).isEqualTo(16.2f);
    }

}
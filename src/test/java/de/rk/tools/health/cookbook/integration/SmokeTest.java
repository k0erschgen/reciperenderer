package de.rk.tools.health.cookbook.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.rk.tools.health.cookbook.Application;
import de.rk.tools.health.cookbook.CookbookController;
import de.rk.tools.health.cookbook.TestUtils;
import de.rk.tools.health.cookbook.data.Recipe;
import de.rk.tools.health.cookbook.data.RecipeIngredient;
import de.rk.tools.health.cookbook.data.RecipeRepository;
import de.rk.tools.health.cookbook.wrapper.RecipeWrapper;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@AutoConfigureMockMvc(addFilters = false)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SmokeTest extends AbstractPersistenceIntegrationTest {

    @Rule
    public TestName testName = new TestName();

    @Autowired
    ObjectMapper mapper;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CookbookController cookbookController;

    @Autowired
    private RecipeRepository recipeRepository;

    private RecipeWrapper recipeWrapper;

    private String chefkochId = "2017651326831340";

    private String chefkochUrl = "https://www.chefkoch.de/rezepte/" + chefkochId + "/Vegane-Tomaten-Quiche.html";

    @Test
    public void _001_testContextLoads() throws Exception {
        System.out.println(System.getProperty("user.country"));
        assertThat(cookbookController).isNotNull();
    }

    @Test
    public void _002_testIndexPage() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("enter chefkoch.de receipe URL here")))
                .andReturn();
        validate(mvcResult);
    }

    @Test
    public void _003_setupDb() throws Exception {
        setupRecipeData();
        setupNutritionData();
    }

    @Test
    public void _004_testStoredRecipes() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/listRecipes.html"))
                .andExpect(status().isOk())
                .andReturn();
        validate(mvcResult);
    }

    @Test
    public void _005_testDisplayRecipeFromChefkochWithMultiplier() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("enter chefkoch.de receipe URL here")));

        MvcResult mvcResult = mockMvc.perform(get("/displayRecipe.html").param("url", chefkochUrl).param("multiplier", "1,5"))
                .andExpect(status().isOk())
                .andReturn();
        validate(mvcResult);
    }

    @Test
    public void _006_testEditAndSaveRecipeFromChefkoch() throws Exception {
        // read index page
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("enter chefkoch.de receipe URL here")));

        // read yet unknown recipe from CK
        MvcResult mvcResult = mockMvc.perform(get("/displayRecipe.html")
                        .param("url", chefkochUrl)
                        .param("multiplier", "1,0"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<span>1</span>\n                    <span> Portion</span>")))
                .andReturn();

        // controller has set the result into HttpSession and not stored in DB yet
        assertThat(recipeRepository.findBySrcId(chefkochId)).isNull();
        recipeWrapper = (RecipeWrapper) mvcResult.getRequest().getSession().getAttribute("lastDraftRecipe");

        // call edit recipe with object from HttpSession
        MvcResult bResult = mockMvc.perform(get("/editRecipe.html")
                        .sessionAttr("lastDraftRecipe", recipeWrapper)
                        .param("recipeId", chefkochId))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>Edit Vegane Tomaten-Quiche</title>")))
                .andReturn();

        String json = serialize(recipeWrapper.getRecipe());

        // store in DB
        assertThat(recipeRepository.findBySrcId(chefkochId)).isNull();
        MvcResult cResult = mockMvc.perform(post("/saveRecipe.html")
                        .param("recipeId", chefkochId)
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk())
                .andReturn();
        assertThat(recipeRepository.findBySrcId(chefkochId)).isNotNull();
        validate(cResult);
    }

    @Test
    public void _007_testCreationFromScratch() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/createRecipe.html"))
                .andExpect(status().is(303))
                .andExpect(redirectedUrl("editRecipe.html?recipeId=000009"))
                .andReturn();
        recipeWrapper = (RecipeWrapper) mvcResult.getRequest().getSession().getAttribute("lastDraftRecipe");

        // call edit recipe with object from HttpSession
        final MvcResult mvcResult1 = mockMvc.perform(get("/editRecipe.html")
                        .sessionAttr("lastDraftRecipe", recipeWrapper)
                        .param("recipeId", "000009"))
                .andExpect(status().isOk())
                .andReturn();
        validate(mvcResult1);
    }

    @Test
    public void _008_deleteRecipe() throws Exception {
        Recipe recipe = recipeRepository.findBySrcId("0008");
        assertThat(recipe).isNotNull();

        mockMvc.perform(get("/deleteRecipe.html")
                        .param("recipeId", "0008"))
                .andExpect(status().is(303))
                .andExpect(redirectedUrl("listRecipes.html"));

        MvcResult mvcResult = mockMvc.perform(get("/listRecipes.html")).andExpect(status().isOk()).andReturn();
        assertThat(mvcResult.getResponse().getContentAsString()).doesNotContain("Veganes Naan Brot aus der Pfanne");
    }

    @Test
    public void _009_deleteIngredientFromRecipe() throws Exception {
        MvcResult aResult = mockMvc.perform(get("/displayRecipe.html")
                        .param("url", "0007")
                        .param("multiplier", "1,0"))
                .andExpect(status().isOk())
                .andReturn();
        String before = aResult.getResponse().getContentAsString();

        Recipe recipe = recipeRepository.findBySrcId("0007");
        String serializedRecipe = mapper.writeValueAsString(recipe);
        Recipe recipe1 = mapper.readValue(serializedRecipe.getBytes(StandardCharsets.UTF_8), Recipe.class);
        recipe1.getRecipeIngredients().remove(3);
        String serializedChangedRecipe = mapper.writeValueAsString(recipe1);

        mockMvc.perform(post("/saveRecipe.html")
                        .param("recipeId", "0007")
                        .contentType(MediaType.APPLICATION_JSON).content(serializedChangedRecipe))
                .andExpect(status().isOk())
                .andReturn();

        MvcResult after = mockMvc.perform(get("/displayRecipe.html")
                        .param("url", "0007")
                        .param("multiplier", "1,0"))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(after.getResponse().getContentAsString()).isNotEqualTo(before);
        validate(after);
    }

    @Test
    public void _010_testEditNewVersion() throws Exception {
        MvcResult resultBefore = mockMvc.perform(get("/listRecipes.html"))
                .andExpect(status().isOk())
                .andReturn();

        String recipeAsJson = mapper.writeValueAsString(recipeRepository.findBySrcId("642041165239332"));
        Recipe recipe = mapper.readValue(recipeAsJson.getBytes(StandardCharsets.UTF_8), Recipe.class);

        MvcResult bResult = mockMvc.perform(post("/saveRecipe.html")
                        .param("recipeId", "642041165239332")
                        .param("parentRecipeId", "642041165239332")
                        .contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(recipe)))
                .andExpect(status().isOk())
                .andReturn();

        MvcResult resultAfter = mockMvc.perform(get("/listRecipes.html"))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(resultBefore.getResponse().getContentAsString()).isNotEqualTo(resultAfter.getResponse().getContentAsString());
        validate(resultAfter);
    }

    @Test
    public void _010a_testEditNewVersionDidNotPersistZeroNutritionAmountValues() throws Exception {
        Recipe version = recipeRepository.findBySrcId("8");
        for (RecipeIngredient recipeIngredient : version.getRecipeIngredients()) {
            assertThat(recipeIngredient.getNutritionAmount()).isNull();
        }
    }

    @Test
    public void _011_testChangeNewVersion() throws Exception {
        String recipeAsJson = mapper.writeValueAsString(recipeRepository.findBySrcId("2478651390137298"));
        Recipe recipe = mapper.readValue(recipeAsJson.getBytes(StandardCharsets.UTF_8), Recipe.class);

        recipe.getRecipeIngredients().get(1).setAmount(125f);
        recipe.getRecipeIngredients().get(7).setAmount(200f);

        MvcResult aResult = mockMvc.perform(post("/saveRecipe.html")
                        .param("recipeId", "2478651390137298")
                        .param("parentRecipeId", "2478651390137298")
                        .contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(recipe)))
                .andExpect(status().isOk())
                .andReturn();

        MvcResult mvcResult = mockMvc.perform(get("/displayRecipe.html")
                        .param("url", "9")
                        .param("multiplier", "1,0"))
                .andExpect(status().isOk())
                .andReturn();

        validate(mvcResult);
    }

    private String getTestName() {
        return this.getClass().getSimpleName() + this.testName.getMethodName();
    }

    private void validate(MvcResult mvcResult, String methodInternalStepPostfix) throws Exception {
        TestUtils.validate(getTestName() + "_" + methodInternalStepPostfix, mvcResult.getResponse().getContentAsString());
    }

    private void validate(MvcResult mvcResult) throws Exception {
        TestUtils.validate(getTestName(), mvcResult.getResponse().getContentAsString());
    }

}

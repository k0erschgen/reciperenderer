package de.rk.tools.health.cookbook.external.fddb;

import de.rk.tools.health.cookbook.data.Recipe;
import de.rk.tools.health.cookbook.data.RecipeIngredient;
import de.rk.tools.health.cookbook.wrapper.RecipeIngredientWrapper;
import de.rk.tools.health.cookbook.wrapper.RecipeWrapper;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

public class FddbMealTest {

    public static final LocalDateTime LOCAL_DATE_TIME = LocalDateTime.of(2020, 4, 4, 13, 34);

    @Test
    public void numOfIngredientsDependsOnFractionalAmount() throws Exception {
        // given
        Recipe recipe = new Recipe();
        ArrayList<RecipeIngredient> ingredients = new ArrayList<>();
        ingredients.add(buildIngredient(8l, 120f, "g")); // OK
        ingredients.add(buildIngredient(143l, 320f, "ml")); // OK
        ingredients.add(buildIngredient(78l, 0.39f, "ml")); // resulting amount too small
        recipe.setRecipeIngredients(ingredients);
        FddbMeal meal = new FddbMeal(
                LOCAL_DATE_TIME,
                new RecipeWrapper(recipe),
                0.25f
        );

        //when
        Collection<RecipeIngredientWrapper> wrappers = meal.getRecipeIngredientWrappers();

        // then
        assertThat(wrappers.size()).isEqualTo(2);
        for (RecipeIngredientWrapper wrapper : wrappers) {
            if (wrapper.getNutritionInfoId().longValue() == 8) {
                assertThat(wrapper.getAmount().floatValue()).isEqualTo(30f);
            }
            if (wrapper.getNutritionInfoId().longValue() == 143) {
                assertThat(wrapper.getAmount().floatValue()).isEqualTo(80f);
            }
        }

    }

    private RecipeIngredient buildIngredient(long nutritionInfoId, float amount, String unit) {
        RecipeIngredient ingredient = new RecipeIngredient();
        ingredient.setNutritionInfoId(nutritionInfoId);
        ingredient.setAmount(amount);
        ingredient.setUnit(unit);
        return ingredient;
    }

}
package de.rk.tools.health.cookbook;

import de.rk.tools.health.cookbook.wrapper.RecipeIngredientWrapper;
import org.assertj.core.data.Offset;
import org.junit.Test;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class UtilsTest {

    @Test
    public void testGetOffsetDateTime() throws Exception {
        long epochSecondFromTest = LocalDateTime.now(Clock.systemDefaultZone()).toEpochSecond(ZoneOffset.UTC);
        long epochSecondFromMethod = Utils.getOffsetLocalDateTime(0.0f).toEpochSecond(ZoneOffset.UTC);
        assertThat(Math.abs(epochSecondFromMethod - epochSecondFromTest)).isBetween(0l, 2l);

        long epochSecondOneHourAgoFromMethod = Utils.getOffsetLocalDateTime(1.0f).toEpochSecond(ZoneOffset.UTC);
        assertThat(epochSecondOneHourAgoFromMethod - epochSecondFromTest).isBetween(3599l, 3601l);

        long epochSecondOneHourLaterFromMethod = Utils.getOffsetLocalDateTime(-1.0f).toEpochSecond(ZoneOffset.UTC);
        assertThat(epochSecondFromTest - epochSecondOneHourLaterFromMethod).isBetween(3599l, 3601l);
    }

    @Test
    public void testAvoidZeroLong() throws Exception {
        assertThat(Utils.avoidZero((Long) null)).isEqualTo("");
        assertThat(Utils.avoidZero(0L)).isEqualTo("");
        assertThat(Utils.avoidZero(1L)).isEqualTo("1");
        assertThat(Utils.avoidZero(-42L)).isEqualTo("-42");
    }

    @Test
    public void testAvoidZeroFloat() throws Exception {
        assertThat(Utils.avoidZero((Float) null)).isEqualTo("");
        assertThat(Utils.avoidZero(0.0f)).isEqualTo("");
        assertThat(Utils.avoidZero(1.0f)).isEqualTo("1.0");
        assertThat(Utils.avoidZero(-42.0f)).isEqualTo("-42.0");
    }

    @Test
    public void testLineBreaksToHtmlMarkup() throws Exception {
        String text = "One\nTwo\rThree\r\nFour\n\nFive";
        assertThat(Utils.handleNewlinesForHtml(text)).isEqualTo("One<br/>Two<br/>Three<br/><br/>Four<br/><br/>Five");
    }

    @Test
    public void testParseAndRefinement() throws Exception {
        RecipeIngredientWrapper brownSugar = Utils.parseIngredient("1 1/2 TL Zucker, braun");
        assertThat(brownSugar.getAmount()).isEqualTo(1.5f);
        assertThat(brownSugar.getUnit()).isEqualTo("TL");
        assertThat(brownSugar.getName()).isEqualTo("Zucker");
        assertThat(brownSugar.getQualifier()).isEqualTo("braun");
        assertThat(brownSugar.getAmountFormatted()).isEqualTo("1 1/2");
    }

    @Test
    public void testReplacementOfUnicodeFractionSymbols() throws Exception {
        for (String frac : Constants.fractionCharMap.keySet()) {
            RecipeIngredientWrapper ingredient = Utils.parseIngredient("1 " + frac + " TL Zucker");
            assertThat(ingredient.getAmount()).isCloseTo(1.0f + Constants.fractionCharValuesMap.get(frac), Offset.offset(0.0001f));
        }
    }

    @Test
    public void testParseIngredient() throws Exception {
        assertThat(Utils.parseIngredient("200 g Paprika").getName()).isEqualTo("Paprika");
        assertThat(Utils.parseIngredient("200 g Paprika").getAmount()).isEqualTo(200.0f);
        assertThat(Utils.parseIngredient("200 g Paprika").getUnit()).isEqualTo("g");

        assertThat(Utils.parseIngredient("Pfeffer, gemahlen").getName()).isEqualTo("Pfeffer");
        assertThat(Utils.parseIngredient("Pfeffer, gemahlen").getQualifier()).isEqualTo("gemahlen");

        assertThat(Utils.parseIngredient("Salz").getName()).isEqualTo("Salz");

        assertThat(Utils.parseIngredient("\u00BD Bund Suppengrün").getAmount()).isCloseTo(0.5f, Offset.offset(0.01f));
        assertThat(Utils.parseIngredient("\u215B Bund Suppengrün").getAmount()).isCloseTo(0.125f, Offset.offset(0.01f));

        assertThat(Utils.parseIngredient("1,00  Zwiebel(n)").getAmount()).isCloseTo(1.0f, Offset.offset(0.01f));
        assertEquals("Zwiebel(n)", Utils.parseIngredient("1,00  Zwiebel(n)").getName());

        assertThat(Utils.parseIngredient("1/2  Zwiebel(n)").getAmount()).isCloseTo(0.5f, Offset.offset(0.01f));
        assertThat(Utils.parseIngredient("1,00  Zwiebel(n)").getName()).isEqualTo("Zwiebel(n)");

        assertThat(Utils.parseIngredient("1/4  Zwiebel(n), klein").getAmount()).isCloseTo(0.25f, Offset.offset(0.01f));
        assertEquals("Zwiebel(n)", Utils.parseIngredient("1,00  Zwiebel(n), klein").getName());
        assertEquals("klein", Utils.parseIngredient("1,00  Zwiebel(n), klein").getQualifier());

        assertThat(Utils.parseIngredient("1,00 Zwiebel(n)").getAmount()).isCloseTo(1.0f, Offset.offset(0.01f));

        assertThat(Utils.parseIngredient("1/2 Bund Petersilie").getAmount()).isCloseTo(0.5f, Offset.offset(0.01f));
        assertEquals("Bund", Utils.parseIngredient("1/2 Bund Petersilie").getUnit());
        assertEquals("Petersilie", Utils.parseIngredient("1/2 Bund Petersilie").getName());

    }


}

package de.rk.tools.health.cookbook.external.fddb;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class FddbNutritionDataTest {

    @Test
    public void testTypeConversionThroughConstructor() {
        assertThat(
                new FddbNutritionData("Ingredient", "200", "ml").getAmount())
                .isEqualTo(200.0f);
        assertThat(
                new FddbNutritionData("Ingredient", "13,5", "g").getAmount())
                .isEqualTo(13.5f);
        assertThat(
                new FddbNutritionData("Ingredient", "13.5", "g").getAmount())
                .isEqualTo(13.5f);
    }

    @Test
    public void testTypeConversionThrowsExceptionWithIllegalChars() {
        try {
            new FddbNutritionData("Ingredient", "13-5", "g");
            fail("Should throw NumberFormatException");
        } catch (Exception e) {
            assertThat(e.getClass()).isEqualTo(NumberFormatException.class);
        }
        try {
            new FddbNutritionData("Ingredient", "13.000,5", "g");
            fail("Should throw NumberFormatException");
        } catch (Exception e) {
            assertThat(e.getClass()).isEqualTo(NumberFormatException.class);
        }
    }

}
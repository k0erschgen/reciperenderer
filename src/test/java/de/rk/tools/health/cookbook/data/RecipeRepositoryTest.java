package de.rk.tools.health.cookbook.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringJUnit4ClassRunner.class)
public class RecipeRepositoryTest {

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private NutritionRepository nutritionRepository;

    @Autowired
    private NutritionDataRepository nutritionDataRepository;

    @Autowired
    private EntityManager entityManager;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setup() throws Exception {
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Test
    public void testNutritionValueAccumulation() throws Exception {
        Nutrition flour = nutritionRepository.save(RepositoryTestUtils.addNutrition(8001l, "Flour"));
        for (NutritionData data : RepositoryTestUtils.addNutritionData(flour, 346f, 28f, 8f)) {
            nutritionDataRepository.save(data);
        }
        Nutrition sugar = nutritionRepository.save(RepositoryTestUtils.addNutrition(8002l, "Sugar"));
        for (NutritionData data : RepositoryTestUtils.addNutritionData(sugar, 100f, 50f, 1f)) {
            nutritionDataRepository.save(data);
        }
        List<Nutrition> nutritions = Arrays.asList(flour, sugar);

        Recipe recipe = RepositoryTestUtils.getSimpleRecipe("9002", "Recipe 1", 2f);
        List<RecipeIngredient> recipeIngredients = RepositoryTestUtils.getSimpleRecipeIngredients(Arrays.asList(
                "400 g Flour", "120 g Sugar"
        ));
        int count = 0;
        for (RecipeIngredient ingredient : recipeIngredients) {
            ingredient.setRecipe(recipe);
            ingredient.setNutritionInfoId(nutritions.get(count).getSrcId());
            count++;
        }
        recipe.setRecipeIngredients(recipeIngredients);
        recipeRepository.save(recipe);

        List<CategoryAndUnitAndAmount> categoryAndUnitAndAmounts = (List<CategoryAndUnitAndAmount>) recipeRepository.collectNutritionValues(recipe.getId());
        assertThat(categoryAndUnitAndAmounts.size()).isEqualTo(3);
        assertThat(categoryAndUnitAndAmounts.get(0).getAmount()).isEqualTo(1504f);
        assertThat(categoryAndUnitAndAmounts.get(2).getAmount()).isEqualTo(33.2f);
    }

    @Test
    public void testSimpleSave() throws Exception {
        String srcId1001 = "1001";
        Recipe recipe1001 = RepositoryTestUtils.getSimpleRecipe(srcId1001, "Recipe 1001", 1.0f);
        recipeRepository.save(recipe1001);
        Recipe persistedRecipe = recipeRepository.findBySrcId(srcId1001);
        assertThat(persistedRecipe.getId()).isGreaterThan(0);
        assertThat(persistedRecipe.getRecipeIngredients().size()).isEqualTo(0);
        assertThat(persistedRecipe.getParentSrcId()).isNull();
    }

    @Test
    public void testSaveWithOneEmptyIngredient() throws Exception {
        String srcId1002 = "1002";
        Recipe initialRecipe1002 = RepositoryTestUtils.getSimpleRecipe(srcId1002, "Recipe 1002", 2.0f);
        initialRecipe1002.setRecipeIngredients(RepositoryTestUtils.getSimpleRecipeIngredients(Arrays.asList(
                "400 g Flour", "50 ml Water", "2 EL Sugar"
        )));
        initialRecipe1002.getRecipeIngredients().forEach(item -> item.setRecipe(initialRecipe1002));
        recipeRepository.save(initialRecipe1002);

        Recipe reloadedRecipe1002 = recipeRepository.findBySrcId(srcId1002);
        assertThat(reloadedRecipe1002.getId()).isGreaterThan(0);
        assertThat(reloadedRecipe1002.getRecipeIngredients().size()).isEqualTo(3);

        String srcId1003 = "1003";
        Recipe newRecipe1003 = RepositoryTestUtils.getSimpleRecipe(srcId1003, "Recipe 1003", 3.0f);
        newRecipe1003.setRecipeIngredients(RepositoryTestUtils.getSimpleRecipeIngredients(Arrays.asList(
                "300 g Pasta", "80 ml Wine", "1 TL Salt", "100 g Tomatoes"
        )));
        newRecipe1003.getRecipeIngredients().add(new RecipeIngredient()); // <- name == null
        newRecipe1003.getRecipeIngredients().forEach(item -> item.setRecipe(newRecipe1003));

        newRecipe1003.setSrcId(reloadedRecipe1002.getSrcId()); // <- * use values (except ingr.)
        newRecipe1003.setId(reloadedRecipe1002.getId());
        newRecipe1003.setName(reloadedRecipe1002.getName());
        newRecipe1003.setServings(reloadedRecipe1002.getServings());
        newRecipe1003.setImgUrl(reloadedRecipe1002.getImgUrl());
        newRecipe1003.setInstructions(reloadedRecipe1002.getInstructions());

        newRecipe1003.getRecipeIngredients().removeIf(ing -> ing.getName() == null); // <- name == null must be removed
        recipeRepository.save(newRecipe1003);

        Recipe persistedRecipe0003 = recipeRepository.findBySrcId(srcId1002); // <- *
        assertThat(persistedRecipe0003.getRecipeIngredients().size()).isEqualTo(4);
    }

    @Test
    public void testSaveAndLoadNewVersions() throws Exception {
        String srcId1004 = "1004";
        Recipe recipe1004 = RepositoryTestUtils.getSimpleRecipe(srcId1004, "Recipe 1004", 1.0f);
        recipeRepository.save(recipe1004);
        Recipe persistedRecipe = recipeRepository.findBySrcId(srcId1004);
        assertThat(persistedRecipe.getId()).isGreaterThan(0);
        assertThat(persistedRecipe.getRecipeIngredients().size()).isEqualTo(0);
        assertThat(persistedRecipe.getParentSrcId()).isNull();

        createNewVersionFrom(srcId1004);
        List<Recipe> recipesV1 = (List<Recipe>) recipeRepository.findAllByParentSrcIdOrderByCreateDateTime(srcId1004);
        assertThat(recipesV1.size()).isEqualTo(1);
        assertThat(recipesV1.get(0).getParentSrcId()).isEqualTo(srcId1004);
        assertThat(recipesV1.get(0).getSrcId()).isEqualTo("1005");

        createNewVersionFrom(srcId1004);
        List<Recipe> recipesV2 = (List<Recipe>) recipeRepository.findAllByParentSrcIdOrderByCreateDateTime(srcId1004);
        assertThat(recipesV2.size()).isEqualTo(2);
        assertThat(recipesV2.get(1).getParentSrcId()).isEqualTo(srcId1004);
        assertThat(recipesV2.get(1).getSrcId()).isEqualTo("1006");
    }

    @Test
    public void saveAndLoadRecipeCheckingType() throws Exception {
        String srcId1006 = "1006";
        Recipe recipe1006 = RepositoryTestUtils.getSimpleRecipe(srcId1006, "Recipe 1006", 1.0f);
        recipeRepository.save(recipe1006);
        entityManager.detach(recipe1006); // needed to make the @PostLoad and @PrePersist work
        Recipe persistedRecipe = recipeRepository.findBySrcId(srcId1006);
        assertThat(persistedRecipe.getRecipeType()).isEqualTo(RecipeType.UNDEFINED);

        String srcId1007 = "1007";
        Recipe recipe1007 = RepositoryTestUtils.getSimpleRecipe(srcId1007, "Recipe 1007", 1.0f);
        recipe1007.setRecipeType(RecipeType.DISH);
        recipeRepository.save(recipe1007);
        entityManager.detach(recipe1007); // needed to make the @PostLoad and @PrePersist work
        Recipe readAgain = recipeRepository.findBySrcId(srcId1007);
        assertThat(readAgain.getRecipeType()).isEqualTo(RecipeType.DISH);
    }

    private void createNewVersionFrom(String fromSrcId) throws java.io.IOException {
        Recipe newVersion = objectMapper.readValue(
                objectMapper.writeValueAsString(
                        recipeRepository.findBySrcId(fromSrcId)), Recipe.class);
        newVersion.setId(null);
        newVersion.setParentSrcId(newVersion.getSrcId());
        newVersion.setSrcId(recipeRepository.findNextFreeRecipeId().toString());
        newVersion.getRecipeIngredients().forEach(recipeIngredient -> {
            recipeIngredient.setId(null);
        });
        recipeRepository.save(newVersion);
    }

}

package de.rk.tools.health.cookbook.external.fddb;

import de.rk.tools.health.cookbook.Application;
import de.rk.tools.health.cookbook.ApplicationConfiguration;
import de.rk.tools.health.cookbook.data.RecipeIngredient;
import de.rk.tools.health.cookbook.wrapper.RecipeIngredientWrapper;
import org.apache.http.NameValuePair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
public class FddbNotepadHandlerTest {

    @Autowired
    ApplicationConfiguration configuration;

    private FddbNotepadHandler handler;

    @Before
    public void setup() throws Exception {
        handler = new FddbNotepadHandler(configuration);
    }

    @Test
    public void testBuildParametersWithRareUnits() throws Exception {
        RecipeIngredientWrapper wrapper1083 = new RecipeIngredientWrapper(buildIngredient(1083, 1f, "Liter"));
        ArrayList<NameValuePair> _1Liter = handler.buildParameters(LocalDateTime.now(),
                wrapper1083.getAmountOrNutritionAmountForCalculations(), wrapper1083.getUnitOrNutritionUnitForCalculations(), "someCrsfToken");
        assertThat(getParamValue(_1Liter, "nicemasshelper")).isEqualTo("1000 ml");
        assertThat(getParamValue(_1Liter, "ownservingmass")).isEqualTo("1000");
        RecipeIngredientWrapper wrapper1083_2 = new RecipeIngredientWrapper(buildIngredient(1083, 1.2f, "kg"));
        ArrayList<NameValuePair> _50Miligrams = handler.buildParameters(LocalDateTime.now(),
                wrapper1083_2.getAmountOrNutritionAmountForCalculations(), wrapper1083_2.getUnitOrNutritionUnitForCalculations(), "someCrsfToken");
        assertThat(getParamValue(_50Miligrams, "nicemasshelper")).isEqualTo("1200 g");
        assertThat(getParamValue(_50Miligrams, "ownservingmass")).isEqualTo("1200");
    }

    private String getParamValue(ArrayList<NameValuePair> parameters, String paramName) {
        String value = null;
        for (NameValuePair parameter : parameters) {
            if (parameter.getName().equals(paramName)) {
                value = parameter.getValue();
                break;
            }
        }
        return value;
    }

    private RecipeIngredient buildIngredient(long nutritionInfoId, float amount, String unit) {
        RecipeIngredient ingredient = new RecipeIngredient();
        ingredient.setNutritionInfoId(nutritionInfoId);
        ingredient.setAmount(amount);
        ingredient.setUnit(unit);
        return ingredient;
    }

}
package de.rk.tools.health.cookbook;

import org.assertj.core.data.Offset;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class FractionTest {

    public static final double SAMPLE_VALUE = 2.230065;
    public static final int SAMPLE_DENOMINATOR = 4;
    public static final double PRECISION_OFFSET = 0.00001;
    public static final String SAMPLE_VALUE_TEXT = "2,23";

    @Test
    public void test() {
        String text = "Angaben für 34 g";
        String ref = text.replaceAll("Angaben für (\\d+) .+", "$1");
        System.out.println(ref);

    }

    @Test
    public void functionalTests() {
        assertThat(Fraction.fractionOf(SAMPLE_VALUE).toString()).isEqualTo("2 1/4");
        assertThat(Fraction.fractionOf(0.4).toString()).isEqualTo("2/5");
        assertThat(Fraction.fractionOf(0.3421886).toString()).isEqualTo("1/3");
        assertThat(Fraction.fractionOf(0.99999999999).toString()).isEqualTo("1");
        assertThat(Fraction.fractionOf(0.5000001).toString()).isEqualTo("1/2");
        assertThat(Fraction.fractionOf(0.501).toString()).isEqualTo("1/2");
        assertThat(Fraction.fractionOf(0.1666).toString()).isEqualTo("1/6");
    }

    @Test
    public void constructorDoesNotSetAllFields() {
        Fraction fraction = new Fraction(SAMPLE_VALUE);
        assertThat(fraction.getWhole()).isEqualTo(0);
        assertThat(fraction.getNumerator()).isEqualTo(0);
        assertThat(fraction.getDenominator()).isEqualTo(0);
        assertThat(fraction.getDecimalNumber()).isEqualTo(SAMPLE_VALUE, Offset.offset(PRECISION_OFFSET));
        assertThat(fraction.toString()).isEqualTo(SAMPLE_VALUE_TEXT);
    }

    @Test
    public void constructorDoesNotRenderAsValid() {
        Fraction fraction = new Fraction(SAMPLE_VALUE);
        assertThat(fraction.isValid()).isFalse();
        assertThat(fraction.getDecimalNumber()).isEqualTo(SAMPLE_VALUE, Offset.offset(PRECISION_OFFSET));
        fraction.setNumeratorAndDenominator(1, SAMPLE_DENOMINATOR);
        assertThat(fraction.isValid()).isTrue();
        assertThat(fraction.toString()).isEqualTo("1/4");
        assertThat(fraction.getDecimalNumber()).isEqualTo(0.25, Offset.offset(PRECISION_OFFSET));
    }

    @Test
    public void settingRemainingFieldsUpdatesWholeIfNumeratorIsGreaterThanDenominator() {
        Fraction fraction = new Fraction(SAMPLE_VALUE);
        assertThat(fraction.getNumerator()).isEqualTo(0);
        assertThat(fraction.getDenominator()).isEqualTo(0);
        assertThat(fraction.getWhole()).isEqualTo(0);
        assertThat(fraction.getDecimalNumber()).isEqualTo(SAMPLE_VALUE, Offset.offset(PRECISION_OFFSET));
        fraction.setNumeratorAndDenominator(9, SAMPLE_DENOMINATOR);
        assertThat(fraction.getWhole()).isEqualTo(2);
        assertThat(fraction.getNumerator()).isEqualTo(1);
        assertThat(fraction.getDenominator()).isEqualTo(SAMPLE_DENOMINATOR);
        assertThat(fraction.toString()).isEqualTo("2 1/4");
        assertThat(fraction.getDecimalNumber()).isEqualTo(2.25, Offset.offset(PRECISION_OFFSET));
    }

    @Test
    public void settingRemainingFieldsLeavesWholeIfNumeratorIsLessThanDenominator() {
        Fraction fraction = new Fraction(0.75);
        assertThat(fraction.getNumerator()).isEqualTo(0);
        assertThat(fraction.getDenominator()).isEqualTo(0);
        assertThat(fraction.getWhole()).isEqualTo(0);
        assertThat(fraction.toString()).isEqualTo("0,75");
        fraction.setNumeratorAndDenominator(3, SAMPLE_DENOMINATOR);
        assertThat(fraction.getWhole()).isEqualTo(0);
        assertThat(fraction.getNumerator()).isEqualTo(3);
        assertThat(fraction.getDenominator()).isEqualTo(SAMPLE_DENOMINATOR);
        assertThat(fraction.toString()).isEqualTo("3/4");
        assertThat(fraction.getDecimalNumber()).isEqualTo(0.75, Offset.offset(PRECISION_OFFSET));
    }

    @Test
    public void settingRemainingFieldsSetsWholeToOneIfNumeratorEqualsDenominator() {
        Fraction fraction = new Fraction(2.0);
        assertThat(fraction.getNumerator()).isEqualTo(0);
        assertThat(fraction.getDenominator()).isEqualTo(0);
        assertThat(fraction.getWhole()).isEqualTo(0);
        assertThat(fraction.toString()).isEqualTo("2");
        assertThat(fraction.getDecimalNumber()).isEqualTo(2.0, Offset.offset(PRECISION_OFFSET));
        fraction.setNumeratorAndDenominator(4, SAMPLE_DENOMINATOR);
        assertThat(fraction.getWhole()).isEqualTo(1);
        assertThat(fraction.getNumerator()).isEqualTo(0);
        assertThat(fraction.getDenominator()).isEqualTo(0);
        assertThat(fraction.toString()).isEqualTo("1");
        assertThat(fraction.getDecimalNumber()).isEqualTo(1.0, Offset.offset(PRECISION_OFFSET));
    }

    @Test
    public void settingRemainingFieldsWithDoubleValues() {
        Fraction fraction = new Fraction(SAMPLE_VALUE);
        fraction.setNumeratorAndDenominator(7.0, 8.0);
        assertThat(fraction.getNumerator()).isEqualTo(7);
        assertThat(fraction.getDenominator()).isEqualTo(8);
        assertThat(fraction.getWhole()).isEqualTo(0);
        assertThat(fraction.toString()).isEqualTo("7/8");
        assertThat(fraction.getDecimalNumber()).isEqualTo(0.875, Offset.offset(PRECISION_OFFSET));

    }

    @Test
    public void settingRemainingFieldsRejectsZeroDenominator() {
        Fraction fraction = new Fraction(SAMPLE_VALUE);
        try {
            fraction.setNumeratorAndDenominator(4, 0);
            fail("Should have thrown an AssertionException!");
        } catch (AssertionError e) {
            assertThat(e).isInstanceOf(AssertionError.class);
        }
        assertThat(fraction.toString()).isEqualTo(SAMPLE_VALUE_TEXT);
    }

    @Test
    public void settingRemainingFieldsRejectsZeroNumerator() {
        Fraction fraction = new Fraction(SAMPLE_VALUE);
        try {
            fraction.setNumeratorAndDenominator(0, 2);
            fail("Should have thrown an AssertionException!");
        } catch (AssertionError e) {
            assertThat(e).isInstanceOf(AssertionError.class);
        }
        assertThat(fraction.toString()).isEqualTo(SAMPLE_VALUE_TEXT);
    }

}
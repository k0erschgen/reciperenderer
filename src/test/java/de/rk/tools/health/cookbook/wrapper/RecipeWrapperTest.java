package de.rk.tools.health.cookbook.wrapper;

import de.rk.tools.health.cookbook.data.NutritionDataHandler;
import de.rk.tools.health.cookbook.data.NutritionRepository;
import de.rk.tools.health.cookbook.data.Recipe;
import de.rk.tools.health.cookbook.data.RecipeIngredient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RecipeWrapperTest {

    @Mock
    private NutritionDataHandler nutritionDataHandler;

    @Mock
    private NutritionRepository nutritionRepository;

    @Before
    public void setup() throws Exception {
        Mockito.reset(nutritionDataHandler, nutritionRepository);
    }

    @Test
    public void testServingsPortioning() throws Exception {
        Recipe recipe = new Recipe();
        recipe.setServings(12f);
        RecipeWrapper wrapper = new RecipeWrapper(recipe);
        assertThat(wrapper.isNetWeightSet()).isFalse();
        assertThat(wrapper.getPortioningAmountFormatted()).isEqualTo("");
        recipe.setNetWeight(825f);
        assertThat(wrapper.isNetWeightSet()).isTrue();
        assertThat(wrapper.getPortioningAmountFormatted()).isEqualTo("68,8");
    }

    @Test
    public void testRelinkIngredients() throws Exception {
        Recipe recipe = new Recipe();
        RecipeIngredient firstInList = new RecipeIngredient();
        RecipeIngredient secondInList = new RecipeIngredient();
        recipe.setRecipeIngredients(Arrays.asList(firstInList, secondInList));

        RecipeWrapper wrapper = new RecipeWrapper(recipe);
        assertThat(wrapper.getRecipeIngredients().get(0).getRecipe()).isNull();
        assertThat(wrapper.getRecipeIngredients().get(1).getRecipe()).isNull();
        wrapper.relinkIngredients();
        assertThat(wrapper.getRecipeIngredients().get(0).getRecipe()).isSameAs(recipe);
        assertThat(wrapper.getRecipeIngredients().get(1).getRecipe()).isSameAs(recipe);
    }

    @Test
    public void testHasValidRecipe() throws Exception {
        RecipeWrapper wrapper = new RecipeWrapper(null);
        assertThat(wrapper.hasValidRecipe()).isFalse();
        RecipeWrapper validWrapper = new RecipeWrapper(new Recipe());
        assertThat(validWrapper.hasValidRecipe()).isTrue();
    }

    @Test
    public void testCleanupIngredients() throws Exception {
        Recipe recipe = new Recipe();
        RecipeIngredient firstInList = new RecipeIngredient();
        firstInList.setName("First");
        RecipeIngredient secondInList = new RecipeIngredient();
        RecipeIngredient thirdInList = new RecipeIngredient();
        thirdInList.setName("Third");
        recipe.setRecipeIngredients(new ArrayList<>(Arrays.asList(firstInList, secondInList, thirdInList)));

        RecipeWrapper wrapper = new RecipeWrapper(recipe);
        assertThat(wrapper.getRecipeIngredients().get(0).getName()).isNotEmpty();
        assertThat(wrapper.getRecipeIngredients().get(1).getName()).isNull();
        assertThat(wrapper.getRecipeIngredients().get(2).getName()).isNotEmpty();
        wrapper.cleanupIngredients();
        assertThat(wrapper.getRecipeIngredients().get(0).getName()).isEqualTo("First");
        assertThat(wrapper.getRecipeIngredients().get(1).getName()).isEqualTo("Third");
    }

    @Test
    public void testInitNutritionHandler() throws Exception {
        Recipe recipe = new Recipe();
        RecipeIngredient firstInList = new RecipeIngredient();
        RecipeIngredient secondInList = new RecipeIngredient();
        firstInList.setNutritionInfoId(23L);
        firstInList.setAmount(200.0f);
        secondInList.setNutritionInfoId(58L);
        secondInList.setNutritionAmount(349.0f);
        recipe.setRecipeIngredients(Arrays.asList(firstInList, secondInList));
        when(nutritionRepository.findBySrcIdIn(
                Arrays.asList(23L, 58L).stream().collect(Collectors.toSet())))
                .thenReturn(Arrays.asList());

        RecipeWrapper wrapper = new RecipeWrapper(recipe);
        assertThat(wrapper.hasNutritionData()).isFalse();
        wrapper.initNutritionDataHandler(nutritionRepository);
        assertThat(wrapper.hasNutritionData()).isTrue();
    }

    @Test
    public void testRenumerateIngredients() throws Exception {
        Recipe recipe = new Recipe();
        RecipeIngredient firstInList = new RecipeIngredient();
        RecipeIngredient secondInList = new RecipeIngredient();
        RecipeIngredient thirdInList = new RecipeIngredient();
        thirdInList.setPosNr(1L);
        thirdInList.setName("Third");
        firstInList.setPosNr(2L);
        firstInList.setName("First");
        secondInList.setPosNr(3L);
        secondInList.setName("Second");
        recipe.setRecipeIngredients(Arrays.asList(firstInList, secondInList, thirdInList));

        RecipeWrapper wrapper = new RecipeWrapper(recipe);
        assertThat(wrapper.getRecipeIngredients().stream().map(RecipeIngredientWrapper::getPosNr).collect(Collectors.toList()))
                .isEqualTo(Arrays.asList(2L, 3L, 1L));
        assertThat(wrapper.getRecipeIngredients().stream().map(RecipeIngredientWrapper::getName).collect(Collectors.toList()))
                .isEqualTo(Arrays.asList("First", "Second", "Third"));

        wrapper.renumerateIngredients();
        assertThat(wrapper.getRecipeIngredients().stream().map(RecipeIngredientWrapper::getPosNr).collect(Collectors.toList()))
                .isEqualTo(Arrays.asList(1L, 2L, 3L));
        assertThat(wrapper.getRecipeIngredients().stream().map(RecipeIngredientWrapper::getName).collect(Collectors.toList()))
                .isEqualTo(Arrays.asList("First", "Second", "Third"));
    }

    @Test
    public void testGetNutritionValueFormatted() throws Exception {
        RecipeIngredient ingredientOne = new RecipeIngredient();
        ingredientOne.setNutritionInfoId(223L);
        ingredientOne.setAmount(400.0f);
        RecipeIngredient ingredientTwo = new RecipeIngredient();
        ingredientTwo.setNutritionInfoId(58L);
        ingredientTwo.setAmount(120.0f);

        when(nutritionDataHandler.getValueFor("Protein", 223L, 400.0f)).thenReturn(20.5f);
        when(nutritionDataHandler.getValueFor("Protein", 58L, 120.0f)).thenReturn(6.2f);
        when(nutritionDataHandler.getValueFor("Protein", 32267L, 0.0f)).thenReturn(0.0f);

        RecipeWrapper recipe = new RecipeWrapper(new Recipe());
        assertThat(recipe.getNutritionValueFormatted("Protein")).isEqualTo("Protein: 0,00");

        recipe.setNutritionDataHandler(nutritionDataHandler);
        assertThat(recipe.getNutritionValueFormatted("Protein")).isEqualTo("Protein: 0,00");

        recipe.setRecipeIngredients(Arrays.asList(ingredientOne, ingredientTwo));
        assertThat(recipe.getNutritionValueFormatted("Protein")).isEqualTo("Protein: 26,70");

        RecipeIngredient ingredientThree = new RecipeIngredient();
        ingredientThree.setNutritionInfoId(32267L);
        ingredientThree.setAmount(null);

        RecipeWrapper recipeTwo = new RecipeWrapper(new Recipe());
        recipeTwo.setNutritionDataHandler(nutritionDataHandler);
        recipeTwo.setRecipeIngredients(Arrays.asList(ingredientOne, ingredientTwo, ingredientThree));
        assertThat(recipeTwo.getNutritionValueFormatted("Protein")).isEqualTo("Protein: 26,70");
    }

}

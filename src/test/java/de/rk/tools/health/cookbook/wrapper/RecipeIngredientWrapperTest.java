package de.rk.tools.health.cookbook.wrapper;

import de.rk.tools.health.cookbook.data.RecipeIngredient;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class RecipeIngredientWrapperTest {

    @Test
    public void testGetUnitOrNutritionUnitForCalculations() throws Exception {
        assertThat(buildWrapperFor("Liter").getUnitOrNutritionUnitForCalculations()).isEqualTo("ml");
        assertThat(buildWrapperFor("dl").getUnitOrNutritionUnitForCalculations()).isEqualTo("ml");
        assertThat(buildWrapperFor("cl").getUnitOrNutritionUnitForCalculations()).isEqualTo("ml");
        assertThat(buildWrapperFor("kg").getUnitOrNutritionUnitForCalculations()).isEqualTo("g");
        assertThat(buildWrapperFor("mg").getUnitOrNutritionUnitForCalculations()).isEqualTo("g");
    }

    @Test
    public void testGetAmountOrNutritionAmountForCalculations() throws Exception {
        assertThat(buildWrapperWith(1.0f, "Liter").getAmountOrNutritionAmountForCalculations()).isEqualTo(1000f);
        assertThat(buildWrapperWith(1.0f, "dl").getAmountOrNutritionAmountForCalculations()).isEqualTo(100f);
        assertThat(buildWrapperWith(1.0f, "cl").getAmountOrNutritionAmountForCalculations()).isEqualTo(10f);
        assertThat(buildWrapperWith(1.0f, "ml").getAmountOrNutritionAmountForCalculations()).isEqualTo(1f);
        assertThat(buildWrapperWith(1.0f, "kg").getAmountOrNutritionAmountForCalculations()).isEqualTo(1000f);
        assertThat(buildWrapperWith(1.0f, "mg").getAmountOrNutritionAmountForCalculations()).isEqualTo(0.001f);
        assertThat(buildWrapperWith(1.0f, "g").getAmountOrNutritionAmountForCalculations()).isEqualTo(1f);
    }

    private RecipeIngredientWrapper buildWrapperWith(float amount, String unit) {
        RecipeIngredientWrapper wrapper = buildWrapperFor(unit);
        wrapper.getRecipeIngredient().setAmount(amount);
        return wrapper;
    }

    private RecipeIngredientWrapper buildWrapperFor(String unit) {
        RecipeIngredient wrapper = new RecipeIngredient();
        wrapper.setUnit(unit);
        return new RecipeIngredientWrapper(wrapper);
    }

}
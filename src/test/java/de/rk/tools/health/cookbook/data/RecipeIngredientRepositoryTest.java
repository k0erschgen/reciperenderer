package de.rk.tools.health.cookbook.data;

import de.rk.tools.health.cookbook.Utils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringJUnit4ClassRunner.class)
public class RecipeIngredientRepositoryTest {

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private RecipeIngredientRepository recipeIngredientRepository;

    @Autowired
    private NutritionRepository nutritionRepository;

    @Autowired
    private NutritionDataRepository nutritionDataRepository;

    @Test
    public void testNutritionValueAccumulation() throws Exception {
        Nutrition nutrition = nutritionRepository.save(RepositoryTestUtils.addNutrition(1001l, "Flour"));
        for (NutritionData nutritionData : RepositoryTestUtils.addNutritionData(nutrition, 237f, 156f, 12f)) {
            nutritionDataRepository.save(nutritionData);
        }
        Recipe recipe = RepositoryTestUtils.getSimpleRecipe("1001", "Recipe", 2f); // recipeRepository.save();

        RecipeIngredient ingredient = Utils.parseIngredient("400 g Flour").getRecipeIngredient();
        ingredient.setNutritionInfoId(nutrition.getSrcId());
        ingredient.setPosNr(1l);
        ingredient.setRecipe(recipe);

        recipe.setRecipeIngredients(Arrays.asList(ingredient));
        recipeRepository.save(recipe);

        Long ingredientDbId = recipe.getRecipeIngredients().get(0).getId();

        List<CategoryAndUnitAndAmount> categoryAndUnitAndAmounts = (List<CategoryAndUnitAndAmount>) recipeIngredientRepository.collectNutritionValues(ingredientDbId);
        assertThat(categoryAndUnitAndAmounts.size()).isEqualTo(3);
        assertThat(categoryAndUnitAndAmounts.get(0).getAmount()).isEqualTo(948f);
        assertThat(categoryAndUnitAndAmounts.get(2).getAmount()).isEqualTo(48f);
    }

}
package de.rk.tools.health.cookbook.external.chefkoch;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import de.rk.tools.health.cookbook.wrapper.RecipeIngredientWrapper;
import de.rk.tools.health.cookbook.wrapper.RecipeWrapper;
import org.junit.Test;

import java.io.StringWriter;
import java.io.Writer;

public class ChefkochReaderTest {

    @Test
    public void testSample() throws Exception {
        String htmlSrc =
                // "https://www.chefkoch.de/rezepte/642041165239332/Linsenbolognese.html";
                "https://www.chefkoch.de/rezepte/2024901328307999/Chili-sin-Carne.html";

        RecipeWrapper recipe = new RecipeWrapper(ChefkochReader.readRecipeFromChefkochUrl(htmlSrc));

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        Writer writer = new StringWriter();
        objectMapper.writeValue(writer, recipe);

        for (RecipeIngredientWrapper ingredient : recipe.getRecipeIngredients()) {
            ingredient.getAmountFormatted();
        }

        new Object();

    }


}
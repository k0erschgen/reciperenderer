package de.rk.tools.health.cookbook.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import de.rk.tools.health.cookbook.data.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AbstractPersistenceIntegrationTest {

    @Autowired
    protected RecipeRepository recipeRepository;

    @Autowired
    protected NutritionRepository nutritionRepository;

    @Autowired
    protected NutritionDataRepository nutritionDataRepository;

    protected ObjectMapper objectMapper;

    private void initObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
            objectMapper.configure(SerializationFeature.WRAP_EXCEPTIONS, true);
        }
    }

    protected String serialize(Recipe recipe) throws Exception {
        initObjectMapper();
        return objectMapper.writeValueAsString(recipe);
    }

    protected void setupRecipeData() throws Exception {
        initObjectMapper();
        List<Path> list = getFilePaths("src/test/resources/data/recipes");
        for (Path path : list) {
            String id = getIdFromFilename(path);
            Recipe recipe = objectMapper.readValue(path.toFile(), Recipe.class);
            recipe.setSrcId(id);
            recipe.setRecipeType(RecipeType.DISH);
            recipe.setInstructions(
                    recipe.getInstructions().replaceAll("\\r\\n", "\\\\n")
            );
            long posNr = 1;
            for (RecipeIngredient recipeIngredient : recipe.getRecipeIngredients()) {
                recipeIngredient.setRecipe(recipe);
                recipeIngredient.setPosNr(posNr);
                posNr++;
            }
            recipeRepository.save(recipe);
        }
    }

    protected void setupNutritionData() throws Exception {
        initObjectMapper();
        List<Path> list = getFilePaths("src/test/resources/data/nutrition");
        for (Path path : list) {
            Long id = Long.parseLong(getIdFromFilename(path));
            Nutrition nutrition = objectMapper.readValue(path.toFile(), Nutrition.class);
            nutrition.setSrcId(id);
            for (NutritionData nutritionData : nutrition.getNutritionData()) {
                nutritionData.setNutrition(nutrition);
            }
            nutritionRepository.save(nutrition);
            new Object();
        }
    }

    private String getIdFromFilename(Path path) {
        return path.toFile().getName().replaceAll("(\\d+).+", "$1");
    }

    private List<Path> getFilePaths(String s) {
        List<Path> list = null;
        try (Stream<Path> paths = Files.walk(Paths.get(s))) {
            list = paths
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}

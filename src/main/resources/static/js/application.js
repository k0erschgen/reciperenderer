function registerIngredientActionEvents() {
    $('a.up, a.down').off('click').on('click', function(event) {
        event.preventDefault();
        var row = $(this).parents('tr:first');
        if ($(this).is('.up')) {
            moveRow(row, 'up');
        } else {
            moveRow(row, 'down');
        }
    });
    $('a.trash').off('click').on('click', function(event) {
        event.preventDefault();
        $(this).closest("tr").remove();
    });
}

function moveRow(row, direction) {
	var curRowIdx = row.index();
	var maxIndexOfRows = row.siblings().length + 1;
	if (direction == 'up') {
		if (curRowIdx > 0) {
			swapPositions(curRowIdx, curRowIdx - 1);
		}
	} else {
		if (curRowIdx < maxIndexOfRows - 1) {
			swapPositions(curRowIdx, curRowIdx + 1);
		}
	}
}

function swapPositions(from, to) {
	var fromRow = $('table.recipeTable tr').eq(from + 1);
	var toRow = $('table.recipeTable tr').eq(to + 1);

	var numOfFields = toRow.find("input").length;

	for (n = 0 ; n < numOfFields ; n++) {
		var toInp = toRow.find("input").eq(n);
		var fromInp = fromRow.find("input").eq(n)

		var toName = toInp.attr('name');
		var fromName = fromInp.attr('name');
		toInp.attr('name', fromName);
		fromInp.attr('name', toName);

		var toVal = toInp.val();
		var fromVal = fromInp.val();
		toInp.val(fromVal);
		fromInp.val(toVal);
	}
}

function calculateMultiplier() {
    if($('#fddb-multiplier').val()) {
       return $('#fddb-multiplier').val() * 1.0;
    }
    if($('#fddb-multiplier-numerator').val() && $('#fddb-multiplier-denominator').val()) {
        return ($('#fddb-multiplier-numerator').val() * 1.0) / ($('#fddb-multiplier-denominator').val() * 1.0);
    }
    if($('#fddb-multiplier-net-weight-part').val() && $('#fddb-multiplier-net-weight-whole').val()) {
        return ($('#fddb-multiplier-net-weight-part').val() * 1.0) / ($('#fddb-multiplier-net-weight-whole').val() * 1.0);
    }
    return 1.0;
}

function calculateTimeOffset() {
    var fieldValue = $('#fddb-time-offset').val().trim();
    return fieldValue.trim() * -1.0;
}

function saveOriginalValues() {
    $('.ingredientRow input').each(function(i, inp) {
        $(inp).attr('data-original-val', $(inp).val());
        console.log($(inp).attr('data-original-val'));
    });
}

function recalcValues() {
    var unchanged = 9999;
    if ($('#link-amounts').is(":checked")) {
        var factor = 1.0;
        var changedIngredientId = unchanged;
        var changedAmountType;
        $('.ingAmountF').each(function(i, inp) {
            var actualVal = $(inp).val();
            var originalVal = $(inp).attr('data-original-val');
            if (actualVal != originalVal) {
                factor = (actualVal * 1.0) / (originalVal * 1.0);
                changedIngredientId = i;
                changedAmountType = 'main';
                return false;
            }
        });
        if (changedIngredientId == unchanged) {
            $('.ingNutriAmountF').each(function(i, inp) {
                var actualVal = $(inp).val();
                var originalVal = $(inp).attr('data-original-val');
                if (actualVal != originalVal) {
                    factor = (actualVal * 1.0) / (originalVal * 1.0);
                    changedIngredientId = i;
                    changedAmountType = 'nutri';
                    return false;
                }
            });
        }
        if (changedIngredientId != unchanged) {
            $('.ingAmountF').each(function(i, inp) {
                if (i != changedIngredientId || changedAmountType == 'nutri') {
                    var actualVal = $(inp).val();
                    $(inp).val((factor * (actualVal * 1.0)).toFixed(2));
                }
            });
            $('.ingNutriAmountF').each(function(i, inp) {
                if (i != changedIngredientId || changedAmountType == 'main') {
                    var actualVal = $(inp).val();
                    $(inp).val((factor * (actualVal * 1.0)).toFixed(2));
                }
            });
            saveOriginalValues();
        }
    }
}

function buildJson() {
    var ingFieldMap = {
        ingNameF: "name",
        ingQualF: "qualifier",
        ingAmountF: "amount",
        ingUnitF: "unit",
        ingNutriF: "nutritionInfo",
        ingNutriAmountF: "nutritionAmount",
        ingNutriUnitF: "nutritionUnit",
        ingPosF: "posNr"
    };
    var myRecipe = new Object();
    var rowCount = 0;
    myRecipe.name = $('#recipe-name').val();
    myRecipe.recipeType = $('#recipe-type').val();
    myRecipe.imgUrl = $('#recipe-imgurl').val();
    myRecipe.srcId = $('#src-id').val();
    myRecipe.servings = $('#recipe-servings').val();
    myRecipe.netWeight = $('#net-weight').val();
    myRecipe.versionName = $('#version-name').val();
    myRecipe.instructions = $('#instructions').val();
    myRecipe.ingredients = [];
    $('.ingredientRow').each(function(i,tr) {
        var myIngredient = new Object();
        rowCount++;
        Object.keys(ingFieldMap).forEach(function(key,index) {
            var theField = $(tr).find("input." + key);
            var iVal = theField.val();
            switch (key) {
                case "ingNameF":
                case "ingQualF":
                case "ingUnitF":
                case "ingNutriUnitF":
                    myIngredient[ingFieldMap[key]] = iVal;
                    break;
                case "ingNutriF":
                    myIngredient[ingFieldMap[key]] = fddbNutritionData[iVal];
                    break;
                case "ingAmountF":
                case "ingNutriAmountF":
                    myIngredient[ingFieldMap[key]] = formatFloat(iVal);
                    break;
                case "ingPosF":
                    if (iVal) {
                        myIngredient[ingFieldMap[key]] = iVal;
                    } else {
                        myIngredient[ingFieldMap[key]] = rowCount;
                    }
                    break;
                default:
                    console.log('unknown field found');
            }
        });
        myRecipe.ingredients.push(myIngredient);
    });
    recipeJson = JSON.stringify(myRecipe);
    console.log(recipeJson);
}

function formatFloat(val) {
    return parseFloat(val);
}

function addNewRow() {
    var lastRow = $('table.recipeTable tr:last');
    lastRow.after('<tr class="ingredientRow">'
        + '<td><input class="ingNameF formValue" type="text" name="ingname" value=""/></td>'
        + '<td><input class="ingQualF formValue" type="text" name="ingqualifier" value=""/></td>'
        + '<td><input class="ingAmountF formValue short" type="text" name="ingamount" value=""/></td>'
        + '<td><input class="ingUnitF formValue short" type="text" name="ingunit" value=""/></td>'
        + '<td><input class="ingNutriF formValue long" list="nutrition" name="nutinfo" value=""/></td>'
        + '<td><input class="ingNutriAmountF formValue short" type="text" name="nutamount" value=""/></td>'
        + '<td><input class="ingNutriUnitF formValue short" type="text" name="nutunit" value=""/></td>'
        + '<input class="ingPosF formValue short" type="hidden" name="ingpos" value=""/>'
        + '<td><a class="up" href="#"><i class="sort-up fa fa-caret-square-o-up"></i></a>'
        + '<a class="down" href="#"><i class="sort-down fa fa-caret-square-o-down"></i></a></td>'
        + '</tr>');
    registerIngredientActionEvents();
}
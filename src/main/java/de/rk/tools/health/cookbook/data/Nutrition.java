package de.rk.tools.health.cookbook.data;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "nutrition")
public class Nutrition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @NotNull
    @Column(unique = true)
    @JsonAlias("id")
    private Long srcId;

    @NotNull
    @Size(max = 200)
    private String ingredientName;

    @NotNull
    private Float referenceAmount;

    @Size(max = 20)
    private String referenceUnit;

    @JsonAlias("entries")
    @OneToMany(mappedBy = "nutrition", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, orphanRemoval = true)
    private List<NutritionData> nutritionData = new ArrayList<>();

    public Nutrition() {
    }

    public Nutrition(@NotNull Long srcId, @NotNull @Size(max = 200) String ingredientName, @NotNull Float referenceAmount, @Size(max = 20) String referenceUnit) {
        this.srcId = srcId;
        this.ingredientName = ingredientName;
        this.referenceAmount = referenceAmount;
        this.referenceUnit = referenceUnit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSrcId() {
        return srcId;
    }

    public void setSrcId(Long srcId) {
        this.srcId = srcId;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public Float getReferenceAmount() {
        return referenceAmount;
    }

    public void setReferenceAmount(Float referenceAmount) {
        this.referenceAmount = referenceAmount;
    }

    public String getReferenceUnit() {
        return referenceUnit;
    }

    public void setReferenceUnit(String referenceUnit) {
        this.referenceUnit = referenceUnit;
    }

    public List<NutritionData> getNutritionData() {
        return nutritionData;
    }

    public void setNutritionData(List<NutritionData> nutritionData) {
        this.nutritionData = nutritionData;
    }
}

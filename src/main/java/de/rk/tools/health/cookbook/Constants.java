package de.rk.tools.health.cookbook;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Constants {

    public static final List<String> DEFAULT_NUTRITION_CATEGORIES = Arrays.asList("Kalorien", "Kohlenhydrate", "Protein", "Fett", "Ballaststoffe", "Zucker", "Kalzium", "Eisen");

    public static final String DEFAULT_IMG_URL = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Foods_%28cropped%29.jpg/1024px-Foods_%28cropped%29.jpg";

    public static String[] UNITS = new String[]{
            "Becher", "Beet/e", "Beutel", "Blatt", "Blätter", "Bund", "Bündel", "cl", "cm",
            "dicke", "dl", "Dose", "Dose/n", "dünne", "Ecke(n)", "Eimer", "einige", "einige Stiele", "EL", "EL, gehäuft", "EL, gestr.",
            "etwas", "evtl.", "extra", "Fässchen", "Fläschchen", "Flasche", "Flaschen", "g", "Glas", "Gläser", "gr. Dose/n", "gr. Flasche(n)",
            "gr. Glas", "gr. Gläser", "gr. Kopf", "gr. Scheibe(n)", "große", "großen", "großer", "großes", "halbe", "Halm(e)", "Handvoll",
            "Kästchen", "kg", "kl. Bund", "kl. Dose/n", "kl. Flasche/n", "kl. Glas", "kl. Gläser", "kl. Kopf", "kl. Scheibe(n)", "kl. Stange(n)", "kl. Stück(e)",
            "kleine", "kleiner", "kleines", "Knolle/n", "Kopf", "Köpfe", "Körner", "Kugel", "Kugel/n", "Kugeln", "Liter", "m.-große", "m.-großer", "m.-großes",
            "mehr", "mg", "ml", "Msp.", "n. B.", "Paar", "Paket", "Pck.", "Pkt.", "Platte/n", "Port.", "Prise(n)", "Prisen", "Prozent %", "Riegel", "Ring/e",
            "Rippe/n", "Rispe(n)", "Rolle(n)", "Schälchen", "Scheibe/n", "Schuss", "Spritzer", "Stange/n", "Stängel", "Staude(n)", "Stiel/e", "Stiele", "Streifen",
            "Stück(e)", "Tablette(n)", "Tafel", "Tafeln", "Tasse", "Tasse/n", "Teil/e", "TL", "TL, gehäuft", "TL, gestr.", "Topf", "Tropfen", "Tube/n", "Tüte/n",
            "viel", "wenig", "Würfel", "Wurzel", "Wurzel/n", "Zehe/n", "Zweig/e"};

    public static List<String> MEASURABLE_UNITS = Arrays.asList(
            "cl", "cm", "dl", "g", "kg", "Liter", "mg", "ml", "Prozent %"
    );

    public static final Map<String, String> fractionCharMap = new HashMap<>();

    static {
        fractionCharMap.put("\u00BD", "1/2");
        fractionCharMap.put("\u2153", "1/3");
        fractionCharMap.put("\u2154", "2/3");
        fractionCharMap.put("\u215B", "1/8");
        fractionCharMap.put("\u215C", "3/8");
        fractionCharMap.put("\u215D", "5/8");
        fractionCharMap.put("\u215E", "7/8");
        fractionCharMap.put("\u00BC", "1/4");
        fractionCharMap.put("\u00BE", "3/4");
        fractionCharMap.put("\u2155", "1/5");
        fractionCharMap.put("\u2156", "2/5");
        fractionCharMap.put("\u2157", "3/5");
        fractionCharMap.put("\u2158", "4/5");
        fractionCharMap.put("\u2159", "1/6");
        fractionCharMap.put("\u215A", "5/6");
    }

    public static final Map<String, Float> fractionCharValuesMap = new HashMap<>();

    static {
        fractionCharValuesMap.put("\u00BD", 0.5f);
        fractionCharValuesMap.put("\u2153", 0.3333f);
        fractionCharValuesMap.put("\u2154", 0.6666f);
        fractionCharValuesMap.put("\u215B", 0.125f);
        fractionCharValuesMap.put("\u215C", 0.375f);
        fractionCharValuesMap.put("\u215D", 0.625f);
        fractionCharValuesMap.put("\u215E", 0.875f);
        fractionCharValuesMap.put("\u00BC", 0.25f);
        fractionCharValuesMap.put("\u00BE", 0.75f);
        fractionCharValuesMap.put("\u2155", 0.2f);
        fractionCharValuesMap.put("\u2156", 0.4f);
        fractionCharValuesMap.put("\u2157", 0.6f);
        fractionCharValuesMap.put("\u2158", 0.8f);
        fractionCharValuesMap.put("\u2159", 0.1666f);
        fractionCharValuesMap.put("\u215A", 0.8333f);
    }


}

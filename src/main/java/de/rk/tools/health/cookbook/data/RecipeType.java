package de.rk.tools.health.cookbook.data;

public enum RecipeType {
    UNDEFINED, DISH, BREAKFAST, DESSERT, PASTRY, SPREAD, SIDE, TOPPING, BEVERAGE, STARTER
}

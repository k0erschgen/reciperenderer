package de.rk.tools.health.cookbook.external.fddb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@JsonPropertyOrder({"ingredientName", "id", "referenceAmount", "entries"})
public class FddbData {

    @JsonProperty
    private String ingredientName;

    @JsonProperty
    private long id;

    @JsonProperty
    private List<FddbNutritionData> entries;

    @JsonProperty
    private float referenceAmount;

    @JsonProperty
    private String referenceUnit;

    @JsonIgnore
    private Map<String, FddbNutritionData> searchMap;

    public FddbData() {
        init();
    }

    public FddbData(String ingredientName, float referenceAmount, long id) {
        this();
        this.id = id;
        this.ingredientName = ingredientName;
        this.referenceAmount = referenceAmount;
    }

    private void init() {
        this.entries = new ArrayList<>();
        this.searchMap = new TreeMap<>();
    }

    public void setEntries(List<FddbNutritionData> entries) {
        init();
        for (FddbNutritionData data : entries) {
            addData(data);
        }
    }

    public void addData(FddbNutritionData data) {
        this.entries.add(data);
        this.searchMap.put(data.getName(), data);
    }

    public FddbData setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
        return this;
    }

    public FddbData setId(long id) {
        this.id = id;
        return this;
    }

    public FddbData setReferenceAmount(float referenceAmount) {
        this.referenceAmount = referenceAmount;
        return this;
    }

    public List<FddbNutritionData> getEntries() {
        return entries;
    }

    public float getValueForRealAmount(FddbNutritionData fddbNutritionData, float realAmount) {
        return fddbNutritionData.getAmount() / referenceAmount * realAmount;
    }

    public FddbNutritionData getFddbDataByIngredientName(String ingredientName) {
        return searchMap.get(ingredientName);
    }

    public String getFormattedValueForRealAmount(String ingredientName, float realAmount) {
        return getFormattedValueForRealAmount("%.0f %s", ingredientName, realAmount);
    }

    private String getFormattedValueForRealAmount(String formatString, String ingredientName, float realAmount) {
        FddbNutritionData fddbNutritionData = getFddbDataByIngredientName(ingredientName);
        return String.format(
                formatString, getValueForRealAmount(fddbNutritionData, realAmount), fddbNutritionData.getUnit()
        );
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public long getId() {
        return id;
    }

    public float getReferenceAmount() {
        return referenceAmount;
    }

    public String getReferenceUnit() {
        return referenceUnit;
    }

    public FddbData setReferenceUnit(String referenceUnit) {
        this.referenceUnit = referenceUnit;
        return this;
    }
}

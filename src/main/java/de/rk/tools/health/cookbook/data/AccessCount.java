package de.rk.tools.health.cookbook.data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "access_count")
public class AccessCount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 30)
    @Column(name = "recipe_src_id", unique = true)
    private String recipeSrcId;

    @NotNull
    private Long counter;

    public AccessCount() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecipeSrcId() {
        return recipeSrcId;
    }

    public void setRecipeSrcId(String recipeSrcId) {
        this.recipeSrcId = recipeSrcId;
    }

    public Long getCounter() {
        return counter;
    }

    public void setCounter(Long counter) {
        this.counter = counter;
    }
}

package de.rk.tools.health.cookbook.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Service
public class QueryService {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    public List<NameAndSrcIdAndRecipeType> findByParentSrcIdIsNullOrderByAccessCountDesc() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery(
                "SELECT r.name, r.srcId, r.recipeType FROM Recipe r LEFT JOIN AccessCount a ON a.recipeSrcId = r.srcId WHERE r.parentSrcId IS NULL ORDER BY a.counter DESC, r.name ASC");
        List resultList = query.getResultList();
        entityManager.close();
        List<NameAndSrcIdAndRecipeType> result = new ArrayList<>();
        for (Object object : resultList) {
            result.add(new NameAndSrcIdAndRecipeTypeImpl((Object[]) object));
        }
        return result;
    }


}

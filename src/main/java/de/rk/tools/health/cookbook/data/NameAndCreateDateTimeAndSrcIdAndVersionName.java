package de.rk.tools.health.cookbook.data;

import java.time.LocalDateTime;

public interface NameAndCreateDateTimeAndSrcIdAndVersionName {
    String getName();

    LocalDateTime getCreateDateTime();

    String getSrcId();

    String getVersionName();
}

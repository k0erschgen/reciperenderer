package de.rk.tools.health.cookbook.data;

public interface CategoryAndUnit {
    String getCategory();

    String getUnit();
}

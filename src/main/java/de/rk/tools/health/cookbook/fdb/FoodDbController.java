package de.rk.tools.health.cookbook.fdb;

import de.rk.tools.health.cookbook.data.Meal;
import de.rk.tools.health.cookbook.data.MealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/fdb")
public class FoodDbController {

    @Autowired
    private MealRepository mealRepository;

    @GetMapping(value = "meal/{id}")
    public Meal findMealById(@PathVariable("id") Long id) {
        return mealRepository.findById(id).orElseThrow(() -> new FdbResourceNotFoundException("Could not find meal with id " + id));
    }

    @PutMapping(value = "meal/{id}")
    public Meal upsertMeal(@RequestBody Meal newMeal, @PathVariable("id") Long id) {

        // TODO: does not yet work

        return mealRepository.findById(id)
                .map(meal -> {
                    meal.setName(newMeal.getName());
                    meal.setTaken(newMeal.getTaken());
                    meal.setAmount(newMeal.getAmount());
                    meal.setUnit(newMeal.getUnit());
                    meal.setMealValues(newMeal.getMealValues());
                    meal.getMealValues().forEach(value -> value.setMeal(meal));
                    return mealRepository.save(meal);
                })
                .orElseGet(() -> {
                    newMeal.setId(id);
                    return mealRepository.save(newMeal);
                });
    }

}

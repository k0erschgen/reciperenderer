package de.rk.tools.health.cookbook;

import de.rk.tools.health.cookbook.data.RecipeIngredient;
import de.rk.tools.health.cookbook.wrapper.RecipeIngredientWrapper;
import org.jsoup.helper.StringUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("d.M.yy HH:mm");

    public static final boolean isNullOrEmpty(String string) {
        if (string == null) {
            return true;
        }
        return string.trim().isEmpty();
    }

    public static final String handleNewlinesForHtml(String text) {
        return text.replaceAll("[\\n\\r+]", "<br/>");
    }

    public static final String avoidZero(Long number) {
        return (number == null || number == 0) ? "" : Long.toString(number);
    }

    public static final String avoidZero(Float number) {
        return (number == null || number == 0.0f) ? "" : Float.toString(number);
    }

    public static final String formatDateTime(LocalDateTime dateTime) {
        return dateTime.format(DATE_TIME_FORMATTER);
    }

    public static final LocalDateTime getOffsetLocalDateTime(float offsetHours) {
        LocalDateTime localDateTime = LocalDateTime.now();
        return localDateTime.plusSeconds(
                Math.round(offsetHours * 60.0 * 60.0)
        );
    }

    public static RecipeIngredientWrapper parseIngredient(String ingredientString) {
        String cleanedIngredientString = preCleanFractionChars(ingredientString);
        RecipeIngredient ingredient = new RecipeIngredient();
        Pattern p = Pattern.compile("^([0-9,/ ]*) (.*?) ?([^,]*),?(.*)$");
        Matcher m = p.matcher(cleanedIngredientString);
        if (m.matches()) {
            String amount = m.group(1).trim();
            String unit = m.group(2).trim();
            String name = m.group(3).trim();
            String qualifier = m.group(4).trim();

            ingredient.setName(name);
            if (!StringUtil.isBlank(amount)) {
                Float amountVal = handleAmount(m.group(1).trim().replace(',', '.'));
                ingredient.setAmount(amountVal);
            }
            if (!StringUtil.isBlank(unit)) {
                ingredient.setUnit(unit);
            }
            if (!StringUtil.isBlank(qualifier)) {
                ingredient.setQualifier(qualifier);
            }
        } else {
            Pattern xp = Pattern.compile("^([^,]*),?(.*?)$");
            Matcher xm = xp.matcher(cleanedIngredientString);
            if (xm.matches()) {
                String name = xm.group(1).trim();
                String qualifier = xm.group(2).trim();
                ingredient.setName(name);
                if (!StringUtil.isBlank(qualifier)) {
                    ingredient.setQualifier(qualifier);
                }
            }
        }
        if (ingredient.getUnit() == null || ingredient.getName() != null) {
            refineUnitAndName(ingredient);
        }
        return new RecipeIngredientWrapper(ingredient);
    }

    private static String preCleanFractionChars(String ingredientString) {
        String result = ingredientString;
        for (String k : Constants.fractionCharMap.keySet()) {
            result = result.replaceAll(k, Constants.fractionCharMap.get(k));
        }
        return result;
    }

    private static float handleAmount(String amountRawValue) {
        String tmp = amountRawValue;
        float preValue = 0.0f;
        if (tmp.contains(" ")) {
            String[] components = tmp.split(" ");
            if (components.length == 2) {
                preValue = Float.parseFloat(components[0].trim());
                tmp = components[1];
            }
        }
        if (tmp.contains("/")) {
            String[] components = tmp.split("/");
            if (components.length == 2) {
                float x = Float.parseFloat(components[0].trim());
                float y = Float.parseFloat(components[1].trim());
                return ((x / y) + preValue);
            }
        }
        return Float.parseFloat(tmp) + preValue;
    }

    private static void refineUnitAndName(RecipeIngredient ingredient) {
        for (String unitStr : Constants.UNITS) {
            String unitPrefix = unitStr + " ";
            String ingredientName = ingredient.getName();
            if (ingredientName.startsWith(unitPrefix)) {
                ingredient.setUnit(ingredientName.substring(0, unitPrefix.length() - 1));
                ingredient.setName(ingredientName.substring(unitPrefix.length()));
            }
        }
    }

}

package de.rk.tools.health.cookbook.wrapper;

import de.rk.tools.health.cookbook.Constants;
import de.rk.tools.health.cookbook.Fraction;
import de.rk.tools.health.cookbook.data.Recipe;
import de.rk.tools.health.cookbook.data.RecipeIngredient;
import org.springframework.util.ObjectUtils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class RecipeIngredientWrapper {

    private RecipeIngredient recipeIngredient;

    private Float factor;

    public RecipeIngredientWrapper(RecipeIngredient recipeIngredient) {
        this.recipeIngredient = recipeIngredient;
    }

    public RecipeIngredientWrapper(RecipeIngredient recipeIngredient, Float factor) {
        this.recipeIngredient = recipeIngredient;
        this.factor = factor;
    }

    private Float factorize(Float value) {
        if (value == null) {
            return 0.0F;
        }
        return factor != null ? value * factor : value;
    }

    public RecipeIngredient getRecipeIngredient() {
        return recipeIngredient;
    }

    public void setFactor(Float factor) {
        this.factor = factor;
    }

    public Long getId() {
        return recipeIngredient.getId();
    }

    public Long getPosNr() {
        return recipeIngredient.getPosNr();
    }

    public String getName() {
        return recipeIngredient.getName();
    }

    public Float getAmount() {
        return factorize(recipeIngredient.getAmount());
    }

    public String getUnit() {
        return recipeIngredient.getUnit();
    }

    public String getQualifier() {
        return recipeIngredient.getQualifier();
    }

    public Long getNutritionInfoId() {
        return recipeIngredient.getNutritionInfoId();
    }

    public String getNutritionUnit() {
        return recipeIngredient.getNutritionUnit();
    }

    public Float getNutritionAmount() {
        return recipeIngredient.getNutritionAmount();
    }

    public String getUnitOrNutritionUnit() {
        if (getNutritionUnit() != null && !getNutritionUnit().isEmpty()) {
            return getNutritionUnit();
        } else if (getUnit() != null && !getUnit().isEmpty()) {
            return getUnit();
        }
        return "";
    }

    public String getUnitOrNutritionUnitForCalculations() {
        String unit = getUnitOrNutritionUnit();
        String calcUnit = unit;
        if (unit.equals("kg") || unit.equals("mg") || unit.equals("g")) {
            calcUnit = "g";
        } else if (unit.equals("Liter") || unit.equals("ml") || unit.equals("cl") || unit.equals("dl")) {
            calcUnit = "ml";
        }
        return calcUnit;
    }

    public Float getAmountOrNutritionAmount() {
        if (recipeIngredient.getNutritionAmount() != null) {
            return factorize(recipeIngredient.getNutritionAmount());
        } else if (recipeIngredient.getAmount() != null) {
            return factorize(recipeIngredient.getAmount());
        }
        return 0.0f;
    }

    public Float getAmountOrNutritionAmountForCalculations() {
        String unit = getUnitOrNutritionUnit();
        float unitFactor = 1f;
        if (unit.equals("kg") || unit.equals("Liter")) {
            unitFactor = 1000f;
        } else if (unit.equals("cl")) {
            unitFactor = 10f;
        } else if (unit.equals("dl")) {
            unitFactor = 100f;
        } else if (unit.equals("mg")) {
            unitFactor = 0.001f;
        }
        return getAmountOrNutritionAmount() * unitFactor;
    }

    public Recipe getRecipe() {
        return recipeIngredient.getRecipe();
    }

    public String getUnitFormatted() {
        if (ObjectUtils.isEmpty(recipeIngredient.getUnit())) {
            return "";
        }
        return recipeIngredient.getUnit();
    }

    public String getAmountFormatted() {
        if (recipeIngredient.getAmount() == null || recipeIngredient.getAmount() < 0.001f) {
            return "";
        }
        if (Constants.MEASURABLE_UNITS.contains(recipeIngredient.getUnit())) {
            DecimalFormat f = new DecimalFormat("0.##");
            f.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.GERMANY));
            return f.format(factorize(recipeIngredient.getAmount()));
        } else {
            return Fraction.fractionOf(factorize(recipeIngredient.getAmount())).toString();
        }
    }
}

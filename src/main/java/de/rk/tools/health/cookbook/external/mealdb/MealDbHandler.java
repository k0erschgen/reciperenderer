package de.rk.tools.health.cookbook.external.mealdb;

import de.rk.tools.health.cookbook.data.*;
import de.rk.tools.health.cookbook.wrapper.RecipeIngredientWrapper;
import de.rk.tools.health.cookbook.wrapper.RecipeWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class MealDbHandler {

    @Autowired
    private MealRepository mealRepository;

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private RecipeIngredientRepository recipeIngredientRepository;

    public void addToMealDb(RecipeWrapper recipeWrapper, LocalDateTime taken, float factor) {
        List<CategoryAndUnitAndAmount> categoryAndUnitAndAmounts = (List<CategoryAndUnitAndAmount>) recipeRepository.collectNutritionValues(recipeWrapper.getRecipe().getId());
        List<MealValue> mealValues = accumulateMealValues(categoryAndUnitAndAmounts, factor);
        persist(mealValues, recipeWrapper.getName(), taken, "Port.", 1f);
    }

    public void addToMealDb(RecipeIngredientWrapper recipeIngredientWrapper, LocalDateTime taken) {
        List<CategoryAndUnitAndAmount> categoryAndUnitAndAmounts = (List<CategoryAndUnitAndAmount>) recipeIngredientRepository.collectNutritionValues(recipeIngredientWrapper.getId());
        List<MealValue> mealValues = accumulateMealValues(categoryAndUnitAndAmounts, 1f);
        persist(mealValues, recipeIngredientWrapper.getName(), taken, recipeIngredientWrapper.getUnit(), recipeIngredientWrapper.getAmount());
    }

    private void persist(List<MealValue> mealValues, String mealName, LocalDateTime mealTaken, String mealUnit, Float mealAmount) {
        Meal meal = new Meal();
        meal.setName(mealName);
        meal.setTaken(mealTaken);
        meal.setUnit(mealUnit);
        meal.setAmount(mealAmount);
        meal.setMealValues(mealValues);
        meal.getMealValues().forEach(item -> item.setMeal(meal));
        mealRepository.save(meal);
    }

    public static List<MealValue> accumulateMealValues(List<CategoryAndUnitAndAmount> categoryAndUnitAndAmounts, float factor) {
        List<MealValue> mealValues = new ArrayList<>();
        for (CategoryAndUnitAndAmount item : categoryAndUnitAndAmounts) {
            MealValue mealValue = new MealValue();
            mealValue.setName(item.getCategory());
            mealValue.setUnit(item.getUnit());
            mealValue.setAmount(item.getAmount() * factor);
            mealValues.add(mealValue);
        }
        return mealValues;
    }

}

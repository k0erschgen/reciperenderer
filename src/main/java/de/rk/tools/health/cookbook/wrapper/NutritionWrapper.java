package de.rk.tools.health.cookbook.wrapper;

import de.rk.tools.health.cookbook.data.Nutrition;
import de.rk.tools.health.cookbook.data.NutritionData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NutritionWrapper {

    private Nutrition nutrition;

    private Map<String, NutritionData> nutritionDataMap = new HashMap<>();

    public NutritionWrapper(Nutrition nutrition) {
        this.nutrition = nutrition;
    }

    private void fillNutritionMap() {
        for (NutritionData data : nutrition.getNutritionData()) {
            nutritionDataMap.put(data.getCategory(), data);
        }
    }

    public NutritionData getNutritionDataForCategory(String category) {
        if (!nutrition.getNutritionData().isEmpty() && nutritionDataMap.isEmpty()) {
            fillNutritionMap();
        }
        return nutritionDataMap.get(category);
    }

    public Nutrition getNutrition() {
        return nutrition;
    }

    public void setNutritionData(List<NutritionData> nutritionData) {
        nutrition.setNutritionData(nutritionData);
    }

    public Long getId() {
        return nutrition.getId();
    }

    public Long getSrcId() {
        return nutrition.getSrcId();
    }

    public String getIngredientName() {
        return nutrition.getIngredientName();
    }

    public Float getReferenceAmount() {
        return nutrition.getReferenceAmount();
    }

    public String getReferenceUnit() {
        return nutrition.getReferenceUnit();
    }

    public List<NutritionData> getNutritionData() {
        return nutrition.getNutritionData();
    }


}

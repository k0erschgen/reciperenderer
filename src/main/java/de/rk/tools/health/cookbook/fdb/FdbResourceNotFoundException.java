package de.rk.tools.health.cookbook.fdb;

public class FdbResourceNotFoundException extends RuntimeException {

    public FdbResourceNotFoundException(String message) {
        super(message);
    }
}

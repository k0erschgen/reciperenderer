package de.rk.tools.health.cookbook.data;

import de.rk.tools.health.cookbook.wrapper.NutritionWrapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NutritionDataHandler {

    private Map<Long, NutritionWrapper> nutritionMap = new HashMap<>();

    public Float getValueFor(String category, Long nutritionInfoId, Float nutritionAmount) {
        NutritionWrapper nutrition = nutritionMap.get(nutritionInfoId);
        if (nutrition != null) {
            Float referenceAmount = nutrition.getReferenceAmount();
            NutritionData nutritionDataForCategory = nutrition.getNutritionDataForCategory(category);
            if (nutritionDataForCategory != null) {
                Float amount = nutritionDataForCategory.getAmount();
                return amount * nutritionAmount / referenceAmount;
            }
        }
        return 0.0f;
    }

    public NutritionDataHandler(List<NutritionWrapper> nutritions) {
        for (NutritionWrapper nutrition : nutritions) {
            nutritionMap.put(nutrition.getSrcId(), nutrition);
        }
    }
}

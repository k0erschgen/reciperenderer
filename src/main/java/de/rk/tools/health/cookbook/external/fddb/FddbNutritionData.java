package de.rk.tools.health.cookbook.external.fddb;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"name", "amount", "unit"})
public class FddbNutritionData {

    @JsonProperty
    private String name;

    @JsonProperty
    private float amount;

    @JsonProperty
    private String unit;

    public FddbNutritionData(String name, String amount, String unit) {
        this.name = name;
        this.amount = Float.parseFloat(amount.replace(',', '.'));
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public float getAmount() {
        return amount;
    }

    public String getUnit() {
        return unit;
    }

}

package de.rk.tools.health.cookbook.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface NutritionRepository extends CrudRepository<Nutrition, Long> {

    Nutrition findBySrcId(long i);

    List<Nutrition> findBySrcIdIn(Set<Long> ids);

    List<IngredientNameAndSrcId> findByIngredientNameIsNotNullOrderByIngredientName();

}

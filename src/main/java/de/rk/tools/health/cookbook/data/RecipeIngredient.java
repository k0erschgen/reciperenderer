package de.rk.tools.health.cookbook.data;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "recipe_ingredient")
public class RecipeIngredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Long posNr;

    @NotNull
    @Size(max = 200)
    private String name;

    private Float amount;

    @Size(max = 20)
    private String unit;

    @Size(max = 200)
    private String qualifier;

    @JsonAlias({"nutritionInfo", "nutritionInfoId"})
    @Column(name = "nutrition_info")
    private Long nutritionInfoId;

    @Size(max = 20)
    private String nutritionUnit;

    private Float nutritionAmount;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "recipe_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Recipe recipe;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecipeIngredient that = (RecipeIngredient) o;
        return posNr.equals(that.posNr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(posNr);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPosNr() {
        return posNr;
    }

    public void setPosNr(Long posNr) {
        this.posNr = posNr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getQualifier() {
        return qualifier;
    }

    public void setQualifier(String qualifier) {
        this.qualifier = qualifier;
    }

    public Long getNutritionInfoId() {
        return nutritionInfoId;
    }

    public void setNutritionInfoId(Long nutritionInfoId) {
        this.nutritionInfoId = nutritionInfoId;
    }

    public String getNutritionUnit() {
        return nutritionUnit;
    }

    public void setNutritionUnit(String nutritionUnit) {
        this.nutritionUnit = nutritionUnit;
    }

    public Float getNutritionAmount() {
        return nutritionAmount;
    }

    public void setNutritionAmount(Float nutritionAmount) {
        this.nutritionAmount = nutritionAmount;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

}

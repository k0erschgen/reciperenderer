package de.rk.tools.health.cookbook.data;

public interface NameAndSrcIdAndRecipeType {
    String getName();

    String getSrcId();

    String getRecipeType();
}

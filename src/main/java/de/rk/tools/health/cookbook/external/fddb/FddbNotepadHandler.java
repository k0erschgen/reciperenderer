package de.rk.tools.health.cookbook.external.fddb;

import de.rk.tools.health.cookbook.ApplicationConfiguration;
import de.rk.tools.health.cookbook.wrapper.RecipeIngredientWrapper;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class FddbNotepadHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(FddbNotepadHandler.class);

    private static final float SMALLEST_PERMITTED_AMOUNT_IN_GRAMMS = 0.51f;

    private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    private static SimpleDateFormat NICE_DATE_FORMAT = new SimpleDateFormat("EEEE, d. MMMM", Locale.GERMAN);

    private static SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");

    private ApplicationConfiguration configuration;

    public FddbNotepadHandler(ApplicationConfiguration configuration) {
        this.configuration = configuration;
    }

    public void addToNotepad(FddbMeal fddbMeal) throws Exception {
        try (FddbOnlineHandler fddbOnlineHandler = new FddbOnlineHandler(configuration)) {
            CloseableHttpClient httpclient = fddbOnlineHandler.httpclient();
            for (RecipeIngredientWrapper wrapper : fddbMeal.getRecipeIngredientWrappers()) {
                Long nutritionInfoId = wrapper.getNutritionInfoId();
                Float amount = wrapper.getAmountOrNutritionAmountForCalculations();
                if (nutritionInfoId != null && amount > 0.0f) {
                    String unit = wrapper.getUnitOrNutritionUnitForCalculations();
                    if (unit.equals("g")) {
                        amount = Math.max(SMALLEST_PERMITTED_AMOUNT_IN_GRAMMS, amount);
                    }
                    addIngredientToFddbNotepad(httpclient, nutritionInfoId, amount, unit, fddbMeal.getDateTime());
                }
            }
        }
    }

    private void addIngredientToFddbNotepad(CloseableHttpClient httpclient, Long nutritionInfoId, Float amount, String unit, LocalDateTime dateTime) throws Exception {
        LOGGER.info(String.format("Writing to fddb.info: NutritionInfoID: %d, %.2f %s ...", nutritionInfoId, amount, unit));
        HttpPost post = createHttpPost(
                configuration.getFddbNotepadHandlerBaseUri() + nutritionInfoId,
                new UrlEncodedFormEntity(
                        buildParameters(dateTime, amount, unit,
                                readCsrfToken(nutritionInfoId, httpclient))));
        try (CloseableHttpResponse postResponse = httpclient.execute(post)) {
            HttpEntity postResponseEntity = postResponse.getEntity();
            EntityUtils.consume(postResponseEntity);
        }
    }

    HttpPost createHttpPost(String uri, UrlEncodedFormEntity entity) {
        HttpPost post = new HttpPost(uri);
        post.setHeader("Origin", "https://fddb.info");
        post.setHeader("Upgrade-Insecure-Requests", "1");
        post.setHeader("Referer", uri);
        post.setEntity(entity);
        return post;
    }

    ArrayList<NameValuePair> buildParameters(LocalDateTime dateTime, Float amount, String unit, String csrfToken) throws Exception {
        ArrayList<NameValuePair> parameters = new ArrayList<>();
        parameters.add(new BasicNameValuePair("csrftoken",
                csrfToken));
        parameters.add(new BasicNameValuePair("submittonotepad",
                "1"));
        parameters.add(new BasicNameValuePair("nicemasshelper",
                String.format("%.0f", amount) + " " + unit));
        parameters.add(new BasicNameValuePair("nicedatehelper",
                NICE_DATE_FORMAT.format(asDate(dateTime))));
        parameters.add(new BasicNameValuePair("xusedatehelper",
                DATE_FORMAT.format(asDate(dateTime))));
        parameters.add(new BasicNameValuePair("day",
                ""));
        parameters.add(new BasicNameValuePair("ownday_day",
                String.format("%d", dateTime.getDayOfMonth())));
        parameters.add(new BasicNameValuePair("ownday_month",
                String.format("%d", dateTime.getMonthValue())));
        parameters.add(new BasicNameValuePair("ownday_year",
                String.format("%d", dateTime.getYear())));
        parameters.add(new BasicNameValuePair("servingtime",
                TIME_FORMAT.format(asDate(dateTime))));
        parameters.add(new BasicNameValuePair("serving",
                "ownserving"));
        parameters.add(new BasicNameValuePair("ownservingmass",
                String.format("%.0f", amount)));
        return parameters;
    }

    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    private String readCsrfToken(Long id, CloseableHttpClient httpclient) throws Exception {
        String result = "";
        HttpGet detailGet = new HttpGet(configuration.getFddbNotepadHandlerBaseUri() + id.toString());
        CloseableHttpResponse detailresponse = httpclient.execute(detailGet);
        try {
            HttpEntity entity = detailresponse.getEntity();
            result = EntityUtils.toString(entity);
        } finally {
            detailresponse.close();
        }
        Document document = Jsoup.parse(result);
        Element field = document.selectFirst("input[name='csrftoken']");
        return field.attr("value");
    }

}

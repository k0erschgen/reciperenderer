package de.rk.tools.health.cookbook.external.fddb;

import de.rk.tools.health.cookbook.ApplicationConfiguration;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;

public class FddbOnlineHandler implements Closeable {

    private static final Logger LOGGER = LoggerFactory.getLogger(FddbOnlineHandler.class);

    private ApplicationConfiguration configuration;

    private CloseableHttpClient httpclient;

    private BasicCookieStore cookieStore = new BasicCookieStore();

    public FddbOnlineHandler(ApplicationConfiguration configuration) throws Exception {
        this.configuration = configuration;
        logon();
    }

    public CloseableHttpClient httpclient() {
        return httpclient;
    }

    private void logon() throws Exception {
        setCookies();
        httpclient = HttpClients.custom()
                .setDefaultCookieStore(cookieStore)
                .setRedirectStrategy(new LaxRedirectStrategy())
                .setRetryHandler(getRetryHandler())
                .build();
        HttpGet httpget = new HttpGet(configuration.getFddbOnlineReaderPrefetchUrl());
        CloseableHttpResponse prefetchResponse = httpclient.execute(httpget);
        try {
            HttpEntity entity = prefetchResponse.getEntity();
            EntityUtils.consume(entity);
        } finally {
            prefetchResponse.close();
        }
        HttpUriRequest login = RequestBuilder.post()
                .setUri(new URI(configuration.getFddbOnlineReaderLogonUrl()))
                .addParameter(configuration.getFddbOnlineReaderUidParamname(), configuration.getFddbOnlineReaderUid())
                .addParameter(configuration.getFddbOnlineReaderPasswordParamname(), configuration.getFddbOnlineReaderPassword())
                .build();
        CloseableHttpResponse loginResponse = httpclient.execute(login);
        try {
            HttpEntity entity = loginResponse.getEntity();
            EntityUtils.consume(entity);
        } finally {
            loginResponse.close();
        }
    }

    private HttpRequestRetryHandler getRetryHandler() {
        return new HttpRequestRetryHandler() {
            @Override
            public boolean retryRequest(IOException exception, int retryCount, HttpContext context) {
                if (retryCount >= 2) {
                    LOGGER.warn("Maximum tries reached");
                    return false;
                }
                if (exception instanceof org.apache.http.NoHttpResponseException) {
                    LOGGER.warn("No response from server on call number {}", retryCount);
                    return true;
                }
                return false;
            }
        };
    }

    private void setCookies() throws Exception {
        BasicClientCookie cookie = new BasicClientCookie("advcookieconsent", "2");
        cookie.setDomain("fddb.info");
        cookie.setPath("/");
        cookieStore.addCookie(cookie);
    }

    @Override
    public void close() throws IOException {
        httpclient.close();
    }
}

package de.rk.tools.health.cookbook.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface RecipeIngredientRepository extends CrudRepository<RecipeIngredient, Long> {

    @Query(
            value = "select nd.category, nd.unit, " +
                    "sum((coalesce(coalesce(ri.nutrition_amount, ri.amount), 0) / n.reference_amount) * nd.amount) as `amount` " +
                    "from recipe_ingredient ri join nutrition n on ri.nutrition_info = n.src_id " +
                    "join nutrition_data nd on nd.nutrition_id = n.id " +
                    "where ri.id = :ingredientId group by nd.category, nd.unit having sum((coalesce(coalesce(ri.nutrition_amount, ri.amount), 0) / n.reference_amount) * nd.amount) > 0 order by sum(nd.amount) desc",
            nativeQuery = true)
    Collection<CategoryAndUnitAndAmount> collectNutritionValues(@Param("ingredientId") Long ingredientId);

}

package de.rk.tools.health.cookbook.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MealValueRepository extends CrudRepository<MealValue, Long> {
}

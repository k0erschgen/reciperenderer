package de.rk.tools.health.cookbook.data;

public interface IngredientNameAndSrcId {
    String getIngredientName();

    Long getSrcId();
}

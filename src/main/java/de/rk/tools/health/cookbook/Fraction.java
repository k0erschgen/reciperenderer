package de.rk.tools.health.cookbook;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

public class Fraction {

    private static final List<Double> ACCEPTED_DENOMINATORS = Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 8.0, 8.0);

    private static final double MAX_DENOMINATOR = ACCEPTED_DENOMINATORS.get(ACCEPTED_DENOMINATORS.size() - 1);

    private static final double MAX_DEVIATION = 0.03;

    private double decimalNumber;

    private int whole = 0;

    private int numerator = 0;

    private int denominator = 0;

    private double deviation;

    public static Fraction fractionOf(double number) {
        Fraction fraction = new Fraction(number);
        double DezimalZahl = Math.abs(number);
        double numerator = 0.0;
        double denominator = 1.0;
        double N1 = 1.0;
        double D1 = 0.0;
        double N2 = 0.0;
        double D2 = 1.0;
        double a = Math.floor(DezimalZahl);
        double x1 = 1.0;
        double x0 = 1.0;
        double y = Math.abs(DezimalZahl) - a;

        while ((denominator != 0.0) && (numerator / denominator != DezimalZahl)) {
            numerator = a * N1 + N2;
            denominator = a * D1 + D2;
            a = Math.floor(x1 / y);
            x1 = y;
            y = x0 - a * y;
            N2 = N1;
            N1 = numerator;
            D2 = D1;
            D1 = denominator;
            x0 = x1;

            if (denominator > MAX_DENOMINATOR) {
                break;
            }
            if (ACCEPTED_DENOMINATORS.contains(denominator)) {
                double deviation = Math.abs(numerator / denominator - DezimalZahl);
                fraction.setDeviation(deviation);
                if (deviation <= MAX_DEVIATION) {
                    fraction.setNumeratorAndDenominator(numerator, denominator);
                }
            }
        }
        return fraction;
    }

    public Fraction(double decimalNumber) {
        this.decimalNumber = decimalNumber;
    }

    private void normalize() {
        if (numerator > denominator) {
            whole = numerator / denominator;
            numerator = numerator % denominator;
            this.decimalNumber = (double) whole + ((double) numerator / (double) denominator);
        } else if (numerator == denominator) {
            whole = 1;
            numerator = 0;
            denominator = 0;
            this.decimalNumber = (double) whole;
        } else {
            whole = 0;
            this.decimalNumber = (double) numerator / (double) denominator;
        }

    }

    public boolean isValid() {
        return whole > 0 || (numerator > 0 && denominator > 0);
    }

    public void setNumeratorAndDenominator(double numerator, double denominator) {
        setNumeratorAndDenominator((int) numerator, (int) denominator);
    }

    public void setNumeratorAndDenominator(int numerator, int denominator) {
        assert denominator > 0;
        assert numerator > 0;
        this.numerator = numerator;
        this.denominator = denominator;
        normalize();
    }

    public double getDecimalNumber() {
        return decimalNumber;
    }

    public int getWhole() {
        return whole;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public double getDeviation() {
        return deviation;
    }

    public void setDeviation(double deviation) {
        this.deviation = deviation;
    }

    @Override
    public String toString() {
        if (isValid()) {
            if (whole > 0) {
                if (numerator != 0 && denominator != 0) {
                    return String.format("%d %d/%d", whole, numerator, denominator);
                } else {
                    return String.format("%d", whole);
                }
            } else {
                return String.format("%d/%d", numerator, denominator);
            }
        } else {
            return new DecimalFormat("0.###").format(decimalNumber);
        }
    }
}

package de.rk.tools.health.cookbook.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccessCountRepository extends JpaRepository<AccessCount, Long> {

    AccessCount findByRecipeSrcId(String recipeSrcId);

}

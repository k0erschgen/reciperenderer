package de.rk.tools.health.cookbook.external.fddb;

import de.rk.tools.health.cookbook.data.RecipeIngredient;
import de.rk.tools.health.cookbook.wrapper.RecipeIngredientWrapper;
import de.rk.tools.health.cookbook.wrapper.RecipeWrapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FddbMeal {

    private static final float ING_AMOUNT_THRESHOLD = 0.1f;

    private LocalDateTime dateTime;

    private RecipeWrapper recipeWrapper;

    private Float factor;

    public FddbMeal(LocalDateTime dateTime, RecipeWrapper recipeWrapper, Float factor) {
        this.dateTime = dateTime;
        this.recipeWrapper = recipeWrapper;
        this.factor = factor;
    }

    public Collection<RecipeIngredientWrapper> getRecipeIngredientWrappers() {
        List<RecipeIngredientWrapper> calculatedWrappers = new ArrayList<>();
        for (RecipeIngredientWrapper recipeIngredient : recipeWrapper.getRecipeIngredients()) {
            RecipeIngredientWrapper wrapperCopy = copyReCalculatingAmount(recipeIngredient);
            if (wrapperCopy.getAmount() >= ING_AMOUNT_THRESHOLD) {
                calculatedWrappers.add(wrapperCopy);
            }
        }
        return calculatedWrappers;
    }

    private RecipeIngredientWrapper copyReCalculatingAmount(RecipeIngredientWrapper originalWrapper) {
        RecipeIngredient newIngredient = new RecipeIngredient();
        newIngredient.setNutritionInfoId(originalWrapper.getNutritionInfoId());
        newIngredient.setAmount(originalWrapper.getAmountOrNutritionAmountForCalculations() * factor);
        newIngredient.setUnit("g");
        return new RecipeIngredientWrapper(newIngredient);
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

}

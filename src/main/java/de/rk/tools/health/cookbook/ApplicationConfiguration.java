package de.rk.tools.health.cookbook;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("file:${user.home}/cookbook.properties")
public class ApplicationConfiguration {

    @Value("${fdd.online.reader.prefetch.url}")
    private String fddbOnlineReaderPrefetchUrl;

    @Value("${fdd.online.reader.logon.url}")
    private String fddbOnlineReaderLogonUrl;

    @Value("${fdd.online.reader.datafetch.base.url}")
    private String fddbOnlineReaderDatafetchBaseUrl;

    @Value("${fdd.online.reader.uid.paramname}")
    private String fddbOnlineReaderUidParamname;

    @Value("${fdd.online.reader.uid}")
    private String fddbOnlineReaderUid;

    @Value("${fdd.online.reader.password.paramname}")
    private String fddbOnlineReaderPasswordParamname;

    @Value("${fdd.online.reader.password}")
    private String fddbOnlineReaderPassword;

    @Value("${fdd.notepad.handler.baseUri:https://fddb.info/db/i18n/addtonotepad/?lang=de&q=}")
    private String fddbNotepadHandlerBaseUri;

    public String getFddbNotepadHandlerBaseUri() {
        return fddbNotepadHandlerBaseUri;
    }

    public String getFddbOnlineReaderPrefetchUrl() {
        return fddbOnlineReaderPrefetchUrl;
    }

    public String getFddbOnlineReaderLogonUrl() {
        return fddbOnlineReaderLogonUrl;
    }

    public String getFddbOnlineReaderDatafetchBaseUrl() {
        return fddbOnlineReaderDatafetchBaseUrl;
    }

    public String getFddbOnlineReaderUidParamname() {
        return fddbOnlineReaderUidParamname;
    }

    public String getFddbOnlineReaderUid() {
        return fddbOnlineReaderUid;
    }

    public String getFddbOnlineReaderPasswordParamname() {
        return fddbOnlineReaderPasswordParamname;
    }

    public String getFddbOnlineReaderPassword() {
        return fddbOnlineReaderPassword;
    }

}

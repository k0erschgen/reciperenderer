package de.rk.tools.health.cookbook.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {

    Recipe findBySrcId(String srcId);

    Collection<Recipe> findAllByParentSrcIdOrderByCreateDateTime(String parentSrcId);

    Collection<NameAndSrcIdAndRecipeType> findByParentSrcIdIsNullOrderByNameAsc();

    Collection<NameAndCreateDateTimeAndSrcIdAndVersionName> findByParentSrcIdIsNotNullOrderByCreateDateTimeDesc();

    @Query(
            value = "select CONVERT(src_id, INTEGER) + 1 from recipe where LENGTH(src_id) < 7 ORDER BY CONVERT(src_id, INTEGER) DESC LIMIT 1",
            nativeQuery = true)
    Long findNextFreeRecipeId();

    @Query(
            value = "select nd.category, nd.unit, " +
                    "sum((coalesce(coalesce(ri.nutrition_amount, ri.amount), 0) / n.reference_amount) * nd.amount) as `amount` " +
                    "from recipe r join recipe_ingredient ri on ri.recipe_id = r.id " +
                    "join nutrition n on ri.nutrition_info = n.src_id join nutrition_data nd on nd.nutrition_id = n.id " +
                    "where r.id = :recipeId group by nd.category, nd.unit having sum((coalesce(coalesce(ri.nutrition_amount, ri.amount), 0) / n.reference_amount) * nd.amount) > 0 order by sum(nd.amount) desc",
            nativeQuery = true)
    Collection<CategoryAndUnitAndAmount> collectNutritionValues(@Param("recipeId") Long recipeId);
}

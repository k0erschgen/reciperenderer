package de.rk.tools.health.cookbook;

public class TemplateUtilsWrapper {

    public String handleNewlinesForHtml(String content) {
        return Utils.handleNewlinesForHtml(content);
    }

    public String avoidZero(Long number) {
        return Utils.avoidZero(number);
    }

    public String avoidZero(Float number) {
        return Utils.avoidZero(number);
    }

}

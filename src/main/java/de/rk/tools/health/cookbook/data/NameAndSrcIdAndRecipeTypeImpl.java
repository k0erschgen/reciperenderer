package de.rk.tools.health.cookbook.data;

public class NameAndSrcIdAndRecipeTypeImpl implements NameAndSrcIdAndRecipeType {

    private String name;

    private String srcId;

    private String recipeType;

    public NameAndSrcIdAndRecipeTypeImpl(Object[] values) {
        this.name = (String) values[0];
        this.srcId = (String) values[1];
        this.recipeType = ((RecipeType) values[2]).name();
    }

    public String getName() {
        return this.name;
    }

    public String getSrcId() {
        return this.srcId;
    }

    public String getRecipeType() {
        return this.recipeType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSrcId(String srcId) {
        this.srcId = srcId;
    }

    public void setRecipeType(String recipeType) {
        this.recipeType = recipeType;
    }
}

package de.rk.tools.health.cookbook;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.List;
import com.itextpdf.layout.element.ListItem;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import de.rk.tools.health.cookbook.wrapper.RecipeIngredientWrapper;
import de.rk.tools.health.cookbook.wrapper.RecipeWrapper;
import org.springframework.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;

public class PdfGenerator {

    private PdfFont sans;
    private PdfFont sansItalic;
    private PdfFont sansBold;
    private PdfFont serif;
    private PdfFont serifBold;

    private Map<String, String> categoriesAndUnits;
    private ByteArrayOutputStream outputStream;
    private PdfWriter writer;
    private PdfDocument pdf;
    private Document document;

    private RecipeWrapper recipeWrapper;

    public PdfGenerator(Map<String, String> categoriesAndUnits) throws IOException {
        this.categoriesAndUnits = categoriesAndUnits;
        init();
    }

    private void init() throws IOException {
        sans = PdfFontFactory.createFont(StandardFonts.HELVETICA);
        sansItalic = PdfFontFactory.createFont(StandardFonts.HELVETICA_OBLIQUE);
        sansBold = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);
        serif = PdfFontFactory.createFont(StandardFonts.TIMES_ROMAN);
        serifBold = PdfFontFactory.createFont(StandardFonts.TIMES_BOLD);
        outputStream = new ByteArrayOutputStream();
        writer = new PdfWriter(outputStream);
        pdf = new PdfDocument(writer);
        document = new Document(pdf);
    }

    public byte[] writePdf(RecipeWrapper recipeWrapper) {
        this.recipeWrapper = recipeWrapper;
        buildRecipeTitle();
        spacer();
        buildIngredientsList();
        spacer();
        buildRecipeInstructions();
        spacer();
        buildNutritionInfo();
        document.close();
        return outputStream.toByteArray();
    }

    private void spacer() {
        document.add(new Paragraph(" ").setFont(sansItalic).setFontSize(8.0f));
    }

    private void buildNutritionInfo() {
        StringBuilder builder = new StringBuilder();
        if (recipeWrapper.hasNutritionData()) {
            for (String category : Constants.DEFAULT_NUTRITION_CATEGORIES) {
                String nutritionValue = recipeWrapper.getNutritionValueFormatted(category);
                if (StringUtils.hasText(nutritionValue)) {
                    builder.append(nutritionValue).append(" ")
                            .append(categoriesAndUnits.get(category)).append(", ");
                }
            }
        }
        String nutritionText = removeLastCharacters(builder.toString(), 2);
        nutritionText = nutritionText.length() > 0 ? "Pro Portion: " + nutritionText : "";
        document.add(new Paragraph(nutritionText)
                .setFont(sansItalic).setFontSize(8.0f)
                .setTextAlignment(TextAlignment.CENTER)
                .setHorizontalAlignment(HorizontalAlignment.CENTER)
                .setBackgroundColor(ColorConstants.LIGHT_GRAY, 0.25f)
                .setMaxWidth(UnitValue.createPercentValue(80)));
    }

    private void buildRecipeTitle() {
        document.add(new Paragraph(recipeWrapper.getName())
                .setFont(serifBold).setFontSize(16.0f)
                .setTextAlignment(TextAlignment.CENTER)
                .setBackgroundColor(ColorConstants.LIGHT_GRAY, 0.25f));
    }

    private void buildRecipeInstructions() {
        document.add(new Paragraph("Zubereitung")
                .setFont(sansBold).setFontSize(11.0f)
        );
        document.add(new Paragraph(recipeWrapper.getInstructions()).setMultipliedLeading(1.2f).setFont(serif).setFontSize(11.0f));
    }

    private void buildIngredientsList() {
        document.add(new Paragraph(buildServingAndPortioningInformation())
                .setFont(sansBold).setFontSize(11.0f)
        );
        List list = new List().setSymbolIndent(0).setListSymbol("").setFont(sans).setFontSize(10.0f);
        for (RecipeIngredientWrapper ingredient : recipeWrapper.getRecipeIngredients()) {
            StringBuilder builder = new StringBuilder();
            builder
                    .append(ingredient.getAmountFormatted()).append(" ")
                    .append(ingredient.getUnitFormatted()).append(" ")
                    .append(ingredient.getName()).append(" ")
                    .append(ingredient.getQualifier());
            list.add(new ListItem(builder.toString()));
        }
        document.add(list);
    }

    private String buildServingAndPortioningInformation() {
        String portionLabel = recipeWrapper.getServings() == 1f ? "Portion" : "Portionen";
        String servingsFormatted = recipeWrapper.getServingsFormatted();
        String portioningAmount = recipeWrapper.getServings() == 1f ? "" : " (\u00e0 " + recipeWrapper.getPortioningAmountFormatted() + " g)";
        return String.format("Zutaten f\u00fcr %s %s%s", servingsFormatted, portionLabel, portioningAmount);
    }

    private String removeLastCharacters(String string, int numOfChars) {
        if (string.length() < 2) {
            return string;
        }
        return string.substring(0, string.length() - 2);
    }
}

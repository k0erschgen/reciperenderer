package de.rk.tools.health.cookbook.data;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "meal")
public class Meal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @NotNull
    private LocalDateTime taken;

    @NotNull
    @Size(max = 200)
    private String name;

    @NotNull
    private Float amount;

    @NotNull
    @Size(max = 20)
    private String unit;

    @JsonAlias("meal_values")
    @OneToMany(mappedBy = "meal", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, orphanRemoval = true)
    private List<MealValue> mealValues = new ArrayList<>();

    public Meal() {
    }

    public Meal(@NotNull Long id, @NotNull @Size(max = 200) String name, @NotNull Float amount, @NotNull @Size(max = 20) String unit, @NotNull LocalDateTime taken) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.taken = taken;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getTaken() {
        return taken;
    }

    public void setTaken(LocalDateTime taken) {
        this.taken = taken;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public List<MealValue> getMealValues() {
        return mealValues;
    }

    public void setMealValues(List<MealValue> mealValues) {
        this.mealValues = mealValues;
    }
}

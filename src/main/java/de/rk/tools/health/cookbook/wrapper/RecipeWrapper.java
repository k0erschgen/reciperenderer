package de.rk.tools.health.cookbook.wrapper;

import de.rk.tools.health.cookbook.Fraction;
import de.rk.tools.health.cookbook.Utils;
import de.rk.tools.health.cookbook.data.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class RecipeWrapper {

    private Recipe recipe;

    private NutritionDataHandler nutritionDataHandler;

    private Float factor = null;

    public RecipeWrapper(Recipe recipe) {
        this.recipe = recipe;
    }

    public void relinkIngredients() {
        recipe.getRecipeIngredients().forEach(item -> item.setRecipe(recipe));
    }

    public void renumerateIngredients() {
        AtomicLong pos = new AtomicLong(1);
        recipe.getRecipeIngredients().forEach(recipeIngredient -> recipeIngredient.setPosNr(pos.getAndIncrement()));
    }

    public boolean hasValidRecipe() {
        return Objects.nonNull(recipe);
    }

    public void cleanupIngredients() {
        recipe.getRecipeIngredients().removeIf(ing -> ing.getName() == null);
    }

    public void turnEmptyValuesToNull() {
        if (recipe != null && Utils.isNullOrEmpty(recipe.getVersionName())) {
            recipe.setVersionName(null);
        }
    }

    public void replaceZeroNutritionAmountsWithNull() {
        for (RecipeIngredient ingredient : recipe.getRecipeIngredients()) {
            final Float nutritionAmount = ingredient.getNutritionAmount();
            if (nutritionAmount != null && nutritionAmount.floatValue() == 0.0f) {
                ingredient.setNutritionAmount(null);
            }
        }
    }

    public void initNutritionDataHandler(NutritionRepository nutritionRepository) {
        Set<Long> recipeIngredientNutritionInfoIds = getRecipeIngredientNutritionInfoIds();
        List<Nutrition> bySrcIdIn = nutritionRepository.findBySrcIdIn(recipeIngredientNutritionInfoIds);
        nutritionDataHandler = new NutritionDataHandler(
                bySrcIdIn.stream().map(item -> new NutritionWrapper(item)).collect(Collectors.toList()));
    }

    public String getServingsFormatted() {
        return Fraction.fractionOf(factorize(recipe.getServings())).toString();
    }

    public String getPortioningAmountFormatted() {
        return isNetWeightSet() ? String.format("%.1f", recipe.getNetWeight() / recipe.getServings().floatValue()) : "";
    }

    private Float factorize(Float value) {
        return factor != null ? value * factor : value;
    }

    public void setFactor(Float factor) {
        this.factor = factor;
        for (RecipeIngredientWrapper recipeIngredient : getRecipeIngredients()) {
            recipeIngredient.setFactor(factor);
        }
    }

    public void setNutritionDataHandler(NutritionDataHandler nutritionDataHandler) {
        this.nutritionDataHandler = nutritionDataHandler;
    }

    public boolean hasNutritionData() {
        return nutritionDataHandler != null;
    }

    public String getNutritionValueFormatted(String category) {
        float nutritionValue = 0.0f;
        for (RecipeIngredientWrapper recipeIngredient : getRecipeIngredients()) {
            Long nutritionInfoId = recipeIngredient.getNutritionInfoId();
            Float nutritionAmount = recipeIngredient.getAmountOrNutritionAmountForCalculations();
            nutritionValue += nutritionDataHandler.getValueFor(category, nutritionInfoId, nutritionAmount);
        }
        return String.format("%s: %.2f", category, (nutritionValue / getServings()));
    }

    public Set<Long> getRecipeIngredientNutritionInfoIds() {
        return recipe.getRecipeIngredients().stream().map(RecipeIngredient::getNutritionInfoId).collect(Collectors.toSet());
    }

    public boolean hasVersionName() {
        return recipe != null && recipe.getVersionName() != null;
    }

    public String getVersionName() {
        return recipe.getVersionName();
    }

    public void setVersionName(String versionName) {
        recipe.setVersionName(versionName);
    }

    public Float getServings() {
        return factorize(recipe.getServings());
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public Long getId() {
        return recipe.getId();
    }

    public String getSrcId() {
        return recipe.getSrcId();
    }

    public String getName() {
        return recipe.getName();
    }

    public String getImgUrl() {
        return recipe.getImgUrl();
    }

    public String getInstructions() {
        return recipe.getInstructions();
    }

    public List<RecipeIngredientWrapper> getRecipeIngredients() {
        return recipe.getRecipeIngredients().stream().map(item -> new RecipeIngredientWrapper(item, factor)).collect(Collectors.toList());
    }

    public RecipeType getRecipeType() {
        return recipe.getRecipeType();
    }

    public LocalDateTime getCreateDateTime() {
        return recipe.getCreateDateTime();
    }

    public LocalDateTime getUpdateDateTime() {
        return recipe.getUpdateDateTime();
    }

    public boolean hasParentSrcId() {
        return recipe != null && recipe.getParentSrcId() != null;
    }

    public String getParentSrcId() {
        return recipe.getParentSrcId();
    }

    public Float getNetWeight() {
        return recipe.getNetWeight();
    }

    public boolean isNetWeightSet() {
        return recipe.getNetWeight() != null && recipe.getNetWeight() > 0.0f;
    }

    public void setParentSrcId(String parentSrcId) {
        recipe.setParentSrcId(parentSrcId);
    }

    public void setId(Long id) {
        recipe.setId(id);
    }

    public void setSrcId(String srcId) {
        recipe.setSrcId(srcId);
    }

    public void setRecipeIngredients(List<RecipeIngredient> recipeIngredients) {
        recipe.setRecipeIngredients(recipeIngredients);
    }

    public void setName(String name) {
        recipe.setName(name);
    }
}

package de.rk.tools.health.cookbook.external.chefkoch;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.rk.tools.health.cookbook.Utils;
import de.rk.tools.health.cookbook.data.Recipe;
import de.rk.tools.health.cookbook.data.RecipeIngredient;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ChefkochReader {

    private static final String SCHEMA_TYPE_ID = "Recipe";

    private static final String SCRIPT_TYPE_APPLICATION_ID = "script[type=application/ld+json]";

    private static final String INGREDIENTS_TABLE_SELECTOR = "table.ingredients";

    public static final String CHEFKOCH_BASE_URL = "https://www.chefkoch.de/rezepte/";


    public static Recipe readRecipeFromChefkochId(String recipeId) throws Exception {
        return readRecipeFromChefkochUrl(CHEFKOCH_BASE_URL + recipeId);
    }

    public static Recipe readRecipeFromChefkochUrl(String url) throws Exception {
        String recipeSrc = readHtmlSourceFromUrl(url);
        Document htmlDoc = parseHtmlDocFromSource(recipeSrc);
        Elements recipeJson = getRecipeJsonElement(htmlDoc);
        Elements ingredientsTable = getIngredientTableElement(htmlDoc);
        return renderRecipeFromHtmlSource(recipeJson, ingredientsTable);
    }

    public static String readHtmlSourceFromUrl(String url) throws IOException {
        String htmlSrc = IOUtils.toString(
                new URL(url),
                StandardCharsets.UTF_8);
        return htmlSrc;
    }

    private static Recipe renderRecipeFromHtmlSource(Elements recipeJsonElement, Elements ingredientsHtmlTableElement) throws Exception {
        Recipe recipe = new Recipe();
        String jsonRecipeString = parseJsonRecipe(recipeJsonElement);
        addInstructionsAndBasicData(recipe, jsonRecipeString);
        addIngredients(recipe, ingredientsHtmlTableElement);
        return recipe;
    }

    private static Elements getIngredientTableElement(Document htmlDoc) {
        return getHtmlPart(htmlDoc, INGREDIENTS_TABLE_SELECTOR);
    }

    private static Elements getRecipeJsonElement(Document htmlDoc) {
        return getHtmlPart(htmlDoc, SCRIPT_TYPE_APPLICATION_ID);
    }


    private static Document parseHtmlDocFromSource(String htmlSrc) {
        return parseHtml(htmlSrc);
    }


    private static Document parseHtml(String htmlSrc) {
        return Jsoup.parse(htmlSrc);
    }

    private static String parseJsonRecipe(Elements scriptBlocks) {
        for (Element el : scriptBlocks) {
            String json = el.html();
            if (json.contains("\"@type\": \"" + SCHEMA_TYPE_ID + "\"")) {
                return json;
            }
        }
        return null;
    }

    private static void addInstructionsAndBasicData(Recipe recipe, String jsonRecipe) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(jsonRecipe);
        recipe.setName(jsonNode.get("name").asText());
        recipe.setImgUrl(extractImgUrl(jsonNode));
        String recipeInstructions = jsonNode.get("recipeInstructions").asText();
        recipe.setInstructions(recipeInstructions);
        String recipeYield = jsonNode.get("recipeYield").asText();
        recipeYield = getCleanedRecipeYieldContent(recipeYield);
        recipe.setServings(Float.parseFloat(recipeYield));
    }

    private static String extractImgUrl(JsonNode json) {
        if (json.get("image").isArray()) {
            return json.get("image").get(0).asText();
        }
        return json.get("image").asText();
    }

    private static String getCleanedRecipeYieldContent(String dirty) {
        return dirty.replaceAll("\\D+", "");
    }

    private static void addIngredients(Recipe recipe, Elements ingredientsTable) {
        List<RecipeIngredient> ingredientList = new ArrayList<>();
        for (Element row : ingredientsTable.select("tr")) {
            String rowContent = row.text();
            RecipeIngredient ingredient = Utils.parseIngredient(rowContent).getRecipeIngredient();
            ingredientList.add(ingredient);
        }
        recipe.setRecipeIngredients(ingredientList);
    }

    private static Elements getHtmlPart(Document htmlDoc, String s) {
        return htmlDoc.select(s);
    }

    public static String extractCkId(String url) {
        return url.replaceAll("^.*?/(\\d+?)/.*$", "$1");
    }
}

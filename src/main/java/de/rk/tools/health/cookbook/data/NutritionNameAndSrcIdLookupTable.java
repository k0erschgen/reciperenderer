package de.rk.tools.health.cookbook.data;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class NutritionNameAndSrcIdLookupTable {

    private BiMap<String, Long> biDiLookupTable = HashBiMap.create();

    public static NutritionNameAndSrcIdLookupTable getFromNutritionRepository(NutritionRepository nutritionRepository) {
        return new NutritionNameAndSrcIdLookupTable().init(nutritionRepository);
    }

    private NutritionNameAndSrcIdLookupTable init(NutritionRepository nutritionRepository) {
        List<IngredientNameAndSrcId> lookupList = nutritionRepository.findByIngredientNameIsNotNullOrderByIngredientName();
        for (IngredientNameAndSrcId nameAndSrcId : lookupList) {
            biDiLookupTable.put(nameAndSrcId.getIngredientName(), nameAndSrcId.getSrcId());
        }
        return this;
    }

    public Map<String, Long> getNamesAndIdsMap() {
        Map<String, Long> namesAndIdsMap = new TreeMap<>();
        for (String key : biDiLookupTable.keySet()) {
            namesAndIdsMap.put(key, biDiLookupTable.get(key));
        }
        return namesAndIdsMap;
    }

    public String getIngredientNameBySrcId(Long srcId) {
        return biDiLookupTable.inverse().get(srcId);
    }

    public Long getSrcIdByIngredientName(String ingredientName) {
        return biDiLookupTable.get(ingredientName);
    }

}

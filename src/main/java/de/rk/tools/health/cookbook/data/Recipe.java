package de.rk.tools.health.cookbook.data;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "recipe")
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    @NotNull
    @Size(max = 30)
    @Column(unique = true)
    private String srcId;

    @NotNull
    @Size(max = 200)
    @JsonProperty
    private String name;

    @Size(max = 30)
    private String parentSrcId;

    @Size(max = 30)
    private String versionName;

    @NotNull
    @JsonProperty
    @JsonAlias({"yield", "servings"})
    @Column(name = "yield")
    private Float servings;

    @JsonProperty
    private Float netWeight;

    @Size(max = 255)
    @JsonProperty
    private String imgUrl;

    @Enumerated(EnumType.STRING)
    private RecipeType recipeType;

    @Lob
    @JsonProperty
    private String instructions;

    @JsonProperty()
    @JsonAlias({"recipeIngredients", "ingredients"})
    @OneToMany(mappedBy = "recipe", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, orphanRemoval = true)
    @OrderBy(value = "pos_nr ASC")
    private List<RecipeIngredient> recipeIngredients = new ArrayList<>();

    public Recipe() {
    }


    @PreUpdate
    @PrePersist
    private void fillEmptyValues() {
        if (this.recipeType == null) {
            this.recipeType = RecipeType.UNDEFINED;
        }
    }

    @NotNull
    @Column(name = "recipe_type", columnDefinition = "VARCHAR(25), default = 'UNDEFINED'")
    public RecipeType getRecipeType() {
        return recipeType;
    }

    public void setRecipeType(RecipeType recipeType) {
        this.recipeType = recipeType;
    }

    public Float getServings() {
        return servings == null ? 1f : servings;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSrcId() {
        return srcId;
    }

    public void setSrcId(String srcId) {
        this.srcId = srcId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setServings(Float servings) {
        this.servings = servings;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public List<RecipeIngredient> getRecipeIngredients() {
        return recipeIngredients;
    }

    public void setRecipeIngredients(List<RecipeIngredient> recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public String getParentSrcId() {
        return parentSrcId;
    }

    public void setParentSrcId(String parentSrcId) {
        this.parentSrcId = parentSrcId;
    }

    public Float getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(Float netWeight) {
        this.netWeight = netWeight;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }
}

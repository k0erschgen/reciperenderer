package de.rk.tools.health.cookbook.external.fddb;

import de.rk.tools.health.cookbook.ApplicationConfiguration;
import de.rk.tools.health.cookbook.data.Nutrition;
import de.rk.tools.health.cookbook.data.NutritionData;
import de.rk.tools.health.cookbook.wrapper.NutritionWrapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class FddbNutritionDataReader {

    private static final String SELECTOR = "div#content div.mainblock div.leftblock div.standardcontent form table tbody";

    private Pattern valuePattern = Pattern.compile("([0-9,\\.]+) (\\w+)");

    private ApplicationConfiguration configuration;

    public FddbNutritionDataReader(ApplicationConfiguration configuration) {
        this.configuration = configuration;
    }

    public List<NutritionWrapper> readNutritionData(Collection<Long> ids) throws Exception {
        final Map<Long, FddbData> fddbDataMap = readData(ids);
        return fddbDataMap.values().stream().map(this::asNutritionWrapper).collect(Collectors.toList());
    }

    private NutritionWrapper asNutritionWrapper(FddbData fddbData) {
        final NutritionWrapper nutrition = new NutritionWrapper(
                new Nutrition(fddbData.getId(), fddbData.getIngredientName(), fddbData.getReferenceAmount(), fddbData.getReferenceUnit()));
        List<NutritionData> nutritionData = new ArrayList<>();
        for (FddbNutritionData entry : fddbData.getEntries()) {
            nutritionData.add(
                    new NutritionData(
                            entry.getName(), entry.getAmount(), entry.getUnit(), nutrition.getNutrition()
                    )
            );
        }
        nutrition.setNutritionData(nutritionData);
        return nutrition;
    }

    FddbData readData(long id) throws Exception {
        Map<Long, FddbData> dataMap = readData(Arrays.asList(id));
        return dataMap.get(id);
    }

    private Map<Long, FddbData> readData(Collection<Long> ids) throws Exception {
        Map<Long, FddbData> data = new TreeMap<>();
        try (FddbOnlineHandler fddbOnlineHandler = new FddbOnlineHandler(configuration)) {
            CloseableHttpClient httpclient = fddbOnlineHandler.httpclient();
            for (Long id : ids) {
                String content = fetchContent(httpclient, id);
                if (!ObjectUtils.isEmpty(content)) {
                    FddbData fddbData = readHtml(content).setId(id);
                    data.put(id, fddbData);
                }
            }
        }
        return data;
    }

    private String fetchContent(CloseableHttpClient httpclient, long id) throws IOException {
        String result = "";
        HttpGet detailGet = new HttpGet(configuration.getFddbOnlineReaderDatafetchBaseUrl() + id);
        CloseableHttpResponse detailresponse = httpclient.execute(detailGet);
        try {
            HttpEntity entity = detailresponse.getEntity();
            result = EntityUtils.toString(entity);
        } finally {
            detailresponse.close();
        }
        return result;
    }

    private FddbData readHtml(String result) {
        Document document = Jsoup.parse(result);
        FddbData fddbData = new FddbData();
        Element title = document.selectFirst("div#content div.mainblock div.leftblock div.standardcontent form h3");
        String productName = title.text().replaceAll("Angaben korrigieren f\u00fcr das Produkt (.+)", "$1");
        Element tbody = document.selectFirst(SELECTOR);
        int rowCount = 0;
        float referenceAmount = 100.0f;
        String referenceUnit = "";
        for (Element row : tbody.getElementsByTag("tr")) {
            Elements cells = row.getElementsByTag("td");
            if (rowCount == 0) {
                String text = cells.get(0).text();
                String regex = "Angaben f\u00fcr (\\d+) (\\w+)";
                String ref = text.replaceAll(regex, "$1");
                referenceUnit = text.replaceAll(regex, "$2");
                referenceAmount = Float.parseFloat(ref);
            } else {
                String value = cells.get(1).text();
                if (ObjectUtils.isEmpty(value) || value.contains("\u00B9")) {
                    continue;
                }
                String name = cells.get(0).text();
                Matcher m = valuePattern.matcher(value);
                if (m.matches()) {
                    String amount = m.group(1).trim();
                    String unit = m.group(2).trim();
                    fddbData.addData(new FddbNutritionData(name, amount, unit));
                }
            }
            rowCount++;
        }
        fddbData
                .setIngredientName(productName)
                .setReferenceAmount(referenceAmount)
                .setReferenceUnit(referenceUnit);
        return fddbData;
    }
}

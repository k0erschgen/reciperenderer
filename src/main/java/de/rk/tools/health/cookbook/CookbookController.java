package de.rk.tools.health.cookbook;

import de.rk.tools.health.cookbook.data.*;
import de.rk.tools.health.cookbook.external.chefkoch.ChefkochReader;
import de.rk.tools.health.cookbook.external.fddb.FddbMeal;
import de.rk.tools.health.cookbook.external.fddb.FddbNotepadHandler;
import de.rk.tools.health.cookbook.external.fddb.FddbNutritionDataReader;
import de.rk.tools.health.cookbook.external.mealdb.MealDbHandler;
import de.rk.tools.health.cookbook.wrapper.NutritionWrapper;
import de.rk.tools.health.cookbook.wrapper.RecipeWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class CookbookController {

    private final static Logger log = LoggerFactory.getLogger(CookbookController.class);

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private NutritionRepository nutritionRepository;

    @Autowired
    private NutritionDataRepository nutritionDataRepository;

    @Autowired
    private AccessCountRepository accessCountRepository;

    @Autowired
    private QueryService queryService;

    @Autowired
    private ApplicationConfiguration configuration;

    @Autowired
    private HttpSession httpSession;

    @Autowired
    private MealDbHandler mealDbHandler;

    @Value("${spring.application.name}")
    String appName;


    @GetMapping(value = {"index.html", "/"})
    public String start(Model model) throws Exception {
        model.addAttribute("appName", appName);
        return "index";
    }

    @GetMapping(value = {"listRecipes.html"})
    public String readReceipesDb(Model model) throws Exception {
        model.addAttribute("receipes", queryService.findByParentSrcIdIsNullOrderByAccessCountDesc());
        model.addAttribute("recipeVersionsMap", getRecipeVersionsMap());
        model.addAttribute("utils", new TemplateUtilsWrapper());
        return "displayDb";
    }

    private Map<String, List<NameAndCreateDateTimeAndSrcIdAndVersionName>> getRecipeVersionsMap() {
        Map<String, List<NameAndCreateDateTimeAndSrcIdAndVersionName>> map = new HashMap<>();
        Collection<NameAndCreateDateTimeAndSrcIdAndVersionName> recipeVersionsList = recipeRepository.findByParentSrcIdIsNotNullOrderByCreateDateTimeDesc();
        for (NameAndCreateDateTimeAndSrcIdAndVersionName nameAndSrcId : recipeVersionsList) {
            final String key = nameAndSrcId.getName();
            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<NameAndCreateDateTimeAndSrcIdAndVersionName>());
            }
            map.get(key).add(nameAndSrcId);
        }
        return map;
    }

    @GetMapping("editRecipe.html")
    public String editRecipe(@RequestParam(name = "recipeId", required = true) String recipeId, Model model) throws Exception {
        RecipeWrapper recipeWrapper = new RecipeWrapper(recipeRepository.findBySrcId(recipeId));
        if (!recipeWrapper.hasValidRecipe()) {
            recipeWrapper = (RecipeWrapper) httpSession.getAttribute("lastDraftRecipe");
        }
        NutritionNameAndSrcIdLookupTable nutritionLookup = NutritionNameAndSrcIdLookupTable
                .getFromNutritionRepository(nutritionRepository);
        model.addAttribute("recipe", recipeWrapper);
        model.addAttribute("parentRecipeId", recipeWrapper.getParentSrcId());
        model.addAttribute("nutritionLookup", nutritionLookup);
        model.addAttribute("recipeId", ChefkochReader.extractCkId(recipeId));
        model.addAttribute("utils", new TemplateUtilsWrapper());
        return "editRecipe";
    }

    @GetMapping("editVersion.html")
    public String editVersion(@RequestParam(name = "recipeId", required = true) String recipeId, Model model) throws Exception {
        RecipeWrapper recipeWrapper = new RecipeWrapper(recipeRepository.findBySrcId(recipeId));
        NutritionNameAndSrcIdLookupTable nutritionLookup = NutritionNameAndSrcIdLookupTable
                .getFromNutritionRepository(nutritionRepository);
        model.addAttribute("recipe", recipeWrapper);
        model.addAttribute("nutritionLookup", nutritionLookup);
        model.addAttribute("recipeId", ChefkochReader.extractCkId(recipeId));
        model.addAttribute("utils", new TemplateUtilsWrapper());
        return "editVersion";
    }

    @RequestMapping(value = "/saveRecipe.html", consumes = "application/json", method = {RequestMethod.POST, RequestMethod.PUT})
    public String saveRecipe(@RequestBody Recipe recipe,
                             @RequestParam(name = "recipeId", required = true) String recipeId,
                             @RequestParam(name = "parentRecipeId", required = false) String parentRecipeId) throws Exception {
        RecipeWrapper recipeWrapper = new RecipeWrapper(recipe);
        recipeWrapper.cleanupIngredients();

        Long existingRecipeId = null;
        LocalDateTime existingCreateDateTime = null;
        RecipeWrapper originalRecipeWrapper = new RecipeWrapper(recipeRepository.findBySrcId(recipeId));
        if (originalRecipeWrapper.hasValidRecipe()) {
            existingRecipeId = originalRecipeWrapper.getId();
            existingCreateDateTime = originalRecipeWrapper.getCreateDateTime();
        }

        if (ObjectUtils.isEmpty(parentRecipeId)) {
            recipeWrapper.setId(existingRecipeId);
            recipeWrapper.setSrcId(recipeId);
        } else {
            recipeWrapper.setParentSrcId(parentRecipeId);
            if (ObjectUtils.isEmpty(recipeId) || recipeId.equals(parentRecipeId)) {
                recipeWrapper.setSrcId(recipeRepository.findNextFreeRecipeId().toString());
                recipeWrapper.getRecipe().setId(null);
                recipeWrapper.getRecipe().getRecipeIngredients().forEach(recipeIngredient -> {
                    recipeIngredient.setId(null);
                });
            } else {
                recipeWrapper.getRecipe().setCreateDateTime(existingCreateDateTime);
                recipeWrapper.setId(existingRecipeId);
                recipeWrapper.setSrcId(recipeId);
            }
        }
        recipeWrapper.relinkIngredients();
        recipeWrapper.renumerateIngredients();
        recipeWrapper.replaceZeroNutritionAmountsWithNull();
        recipeWrapper.turnEmptyValuesToNull();
        recipeRepository.save(recipeWrapper.getRecipe());
        incrementAccessCount(recipeWrapper);
        return "index";
    }

    @GetMapping("displayRecipe.html")
    public String displayRecipe(@RequestParam(name = "url", required = true) String url,
                                @RequestParam(name = "multiplier", required = true) String multiplier, Model model)
            throws Exception {
        String chefkochId = ChefkochReader.extractCkId(url);
        float numericFactor = Float.parseFloat(multiplier.replace(',', '.'));
        RecipeWrapper recipeWrapper = new RecipeWrapper(recipeRepository.findBySrcId(chefkochId));
        if (!recipeWrapper.hasValidRecipe()) {
            recipeWrapper = new RecipeWrapper(ChefkochReader.readRecipeFromChefkochId(chefkochId));
            httpSession.setAttribute("lastDraftRecipe", recipeWrapper);
            model.addAttribute("storedRecipe", false);
        } else {
            recipeWrapper.initNutritionDataHandler(nutritionRepository);
            model.addAttribute("storedRecipe", true);
        }
        recipeWrapper.setFactor(numericFactor);

        model.addAttribute("isVersion", recipeWrapper.hasParentSrcId());
        model.addAttribute("recipe", recipeWrapper);
        model.addAttribute("chefkochId", chefkochId);
        model.addAttribute("categories", Constants.DEFAULT_NUTRITION_CATEGORIES);
        model.addAttribute("categoriesAndUnits", readCategoriesAndUnitsFromRepository());
        model.addAttribute("multiplier", multiplier);
        model.addAttribute("utils", new TemplateUtilsWrapper());
        return "displayRecipe";
    }


    @GetMapping("createRecipe.html")
    public RedirectView createRecipe(RedirectAttributes redirectAttributes) {
        String srcId = findNextFreeRecipeId();
        Recipe recipe = new Recipe();
        recipe.setSrcId(srcId);
        recipe.setName("Neues Rezept " + srcId);
        recipe.setServings(1.0f);
        recipe.setImgUrl(Constants.DEFAULT_IMG_URL);
        RecipeWrapper recipeWrapper = new RecipeWrapper(recipe);
        httpSession.setAttribute("lastDraftRecipe", recipeWrapper);
        redirectAttributes.addAttribute("recipeId", srcId);
        return new RedirectView("editRecipe.html", true, false);
    }

    @GetMapping("deleteRecipe.html")
    public RedirectView deleteRecipe(@RequestParam(name = "recipeId", required = true) String recipeId) {
        Recipe recipe = recipeRepository.findBySrcId(recipeId);
        if (recipe != null) {
            recipeRepository.deleteById(recipe.getId());
        }
        return new RedirectView("listRecipes.html", true, false);
    }

    @GetMapping("postToFddb.html")
    public RedirectView postToFddb(
            @RequestParam(name = "recipeId", required = true) String recipeId,
            @RequestParam(name = "multiplier", required = true) String multiplier,
            @RequestParam(name = "timeOffset", required = true) String timeOffset) throws Exception {
        float factor = Float.parseFloat(multiplier.replace(',', '.'));
        float offsetHrs = Float.parseFloat(timeOffset.replace(',', '.'));
        RecipeWrapper recipeWrapper = new RecipeWrapper(recipeRepository.findBySrcId(recipeId));
        logMealInFddb(factor, offsetHrs, recipeWrapper);
        logMeal(factor, offsetHrs, recipeWrapper);
        incrementAccessCount(recipeWrapper);
        return new RedirectView("listRecipes.html", true, false);
    }

    private void logMeal(float factor, float offsetHrs, RecipeWrapper recipeWrapper) throws Exception {
        mealDbHandler.addToMealDb(recipeWrapper, Utils.getOffsetLocalDateTime(offsetHrs), factor);
    }

    private void logMealInFddb(float factor, float offsetHrs, RecipeWrapper recipeWrapper) throws Exception {
        FddbMeal mealToPost = new FddbMeal(Utils.getOffsetLocalDateTime(offsetHrs), recipeWrapper, factor);
        FddbNotepadHandler fddbNotepadHandler = new FddbNotepadHandler(configuration);
        fddbNotepadHandler.addToNotepad(mealToPost);
    }

    @GetMapping("editNutritionData.html")
    public String editNutritionData(Model model) {
        List<IngredientNameAndSrcId> ingredientNameAndSrcIds = nutritionRepository.findByIngredientNameIsNotNullOrderByIngredientName();
        model.addAttribute("ingredientNameAndSrcIds", ingredientNameAndSrcIds);
        return "editNutritionData";
    }

    @PostMapping("saveNutritionData.html")
    public String saveNutritionData(@RequestBody(required = true) String nutritionDataIds) {
        try {
            String idsAsString = URLDecoder.decode(nutritionDataIds, StandardCharsets.UTF_8.name())
                    .replace("nutritionDataIds=", "");
            if (!ObjectUtils.isEmpty(idsAsString)) {
                List<Long> ids = Arrays.asList(idsAsString.split(","))
                        .stream().map(String::trim).map(Long::parseLong).collect(Collectors.toList());
                List<NutritionWrapper> nutritions = new FddbNutritionDataReader(configuration).readNutritionData(ids);
                for (NutritionWrapper nutrition : nutritions) {
                    if (nutritionRepository.findBySrcId(nutrition.getSrcId()) == null) {
                        nutritionRepository.save(nutrition.getNutrition());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "index";
    }

    @GetMapping(
            value = "/getPdf/{recipeId}",
            produces = MediaType.APPLICATION_PDF_VALUE
    )
    public @ResponseBody
    HttpEntity<byte[]> getPdf(@PathVariable(name = "recipeId", required = true) String recipeId,
                              @RequestParam(name = "multiplier", required = true) String multiplier) throws IOException {
        float numericFactor = Float.parseFloat(multiplier.replace(',', '.'));
        RecipeWrapper recipeWrapper = new RecipeWrapper(recipeRepository.findBySrcId(recipeId));
        recipeWrapper.setFactor(numericFactor);
        recipeWrapper.initNutritionDataHandler(nutritionRepository);

        PdfGenerator pdfGenerator = new PdfGenerator(readCategoriesAndUnitsFromRepository());
        byte[] bytes = pdfGenerator.writePdf(recipeWrapper);

        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "pdf"));
        header.set("Content-Disposition", "inline; filename=" + recipeWrapper.getName());
        header.setContentLength(bytes.length);
        return new HttpEntity<byte[]>(bytes, header);
    }

    private void incrementAccessCount(RecipeWrapper recipeWrapper) {
        String parentRecipeSrcId = recipeWrapper.getParentSrcId() != null ? recipeWrapper.getParentSrcId() : recipeWrapper.getSrcId();
        AccessCount accessCount = accessCountRepository.findByRecipeSrcId(parentRecipeSrcId);
        if (accessCount != null) {
            accessCount.setCounter(accessCount.getCounter() + 1);
        } else {
            accessCount = new AccessCount();
            accessCount.setCounter(1l);
            accessCount.setRecipeSrcId(parentRecipeSrcId);
        }
        accessCountRepository.save(accessCount);
    }

    private Map<String, String> readCategoriesAndUnitsFromRepository() {
        return nutritionDataRepository.findDistinctCategoryAndUnits().stream()
                .collect(Collectors.toMap(
                        CategoryAndUnit::getCategory,
                        CategoryAndUnit::getUnit,
                        (oldValue, newValue) -> oldValue));
    }

    private String findNextFreeRecipeId() {
        return String.format("%06d", recipeRepository.findNextFreeRecipeId());
    }
}

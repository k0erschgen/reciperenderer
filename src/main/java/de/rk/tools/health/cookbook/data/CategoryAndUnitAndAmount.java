package de.rk.tools.health.cookbook.data;

public interface CategoryAndUnitAndAmount {
    String getCategory();

    String getUnit();

    Float getAmount();
}

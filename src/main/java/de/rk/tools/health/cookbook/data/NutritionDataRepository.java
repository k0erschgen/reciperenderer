package de.rk.tools.health.cookbook.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NutritionDataRepository extends CrudRepository<NutritionData, Long> {

    @Query(value = "SELECT DISTINCT category, unit FROM nutrition_data", nativeQuery = true)
    List<CategoryAndUnit> findDistinctCategoryAndUnits();

}

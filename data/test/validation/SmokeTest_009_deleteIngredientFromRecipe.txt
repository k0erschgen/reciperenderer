<!doctype html>
<html lang="de">
    <head>
        <title>Kürbis-Mandel-Rosinen Pilaw</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Slab" rel="stylesheet">
        <link href="css/layout.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="js/jquery-3.4.1.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/application.js"></script>
        <script>
            var recipeId = "0007";
            var dlgMultiplier = 1.0;
        </script>
    </head>
    <body>
        <header>
            <div class="top-actions-panel">
                <a href="/"><i class="home-button fa fa-home fa-2x"></i></a>
                <span class="button-wrapper">
                    <a href="/editRecipe.html?recipeId=0007">
                        <i class="edit-button fa fa-pencil-square-o fa-2x"></i></a></span>
                <span class="button-wrapper">
                    <a href="/editVersion.html?recipeId=0007">
                        <i class="calc-button fa fa-calculator fa-2x"></i></a></span>
                <span class="button-wrapper">
                    <a href="/getPdf/0007?multiplier=1,0">
                        <i class="pdf-button fa fa-file-pdf-o fa-2x"></i></a></span>
                <span class="button-wrapper">
                    <a id="post-to-fddb-button" href="#">
                        <i class="fddb-button fa fa-cutlery fa-2x"></i></a></span>
                <span class="button-wrapper">
                    <a href="/deleteRecipe.html?recipeId=0007">
                        <i class="delete-button fa fa-trash-o fa-2x"></i></a></span>
            </div>
            <div class="header-block">
                <div class="instructionsHeadline">
                    <span>Kürbis-Mandel-Rosinen Pilaw</span>
                </div>
                <div class="recipeTypeContainer"><span>Hauptgericht</span></div>
                <div class="yield">
                    <a href="/displayRecipe.html?url=0007&amp;multiplier=0.25">&#xBC;</a>
                    <a href="/displayRecipe.html?url=0007&amp;multiplier=0.33">&frac13;</a>
                    <a href="/displayRecipe.html?url=0007&amp;multiplier=0.5">&#xBD;</a>
                    <a href="/displayRecipe.html?url=0007&amp;multiplier=0.66">&frac23;</a>
                    <a href="/displayRecipe.html?url=0007&amp;multiplier=0.75">&#xBE;</a>
                    <a href="/displayRecipe.html?url=0007&amp;multiplier=1.0">1x</a>
                    <a href="/displayRecipe.html?url=0007&amp;multiplier=1.5">1&#xBD;x</a>
                    <a href="/displayRecipe.html?url=0007&amp;multiplier=2.0">2x</a>
                    <a href="/displayRecipe.html?url=0007&amp;multiplier=2.5">2&#xBD;x</a>
                </div>
                <div class="yield-portions">
                    <span>4</span>
                    <span> Portion</span><span>en</span>
                </div>
            </div>
        </header>
        <section>
            <nav class="ingredients-panel">
                <ul class="multiplier-panel">
                    <form class="multiplier-form">
    <table class="multiplier-table">
        <tr class="multiplier-table-row">
            <td class="multiplier-td-label">Fraction:</td>
            <td class="multiplier-td-content">
                <input type="text" class="fddb-multiplier numerator" name="fddb-multiplier-numerator" id="fddb-multiplier-numerator">
                /
                <input type="text" class="fddb-multiplier denominator" name="fddb-multiplier-denominator" id="fddb-multiplier-denominator">
            </td>
        </tr>
        <tr class="multiplier-table-row">
            <td class="multiplier-td-label">Decimal val.:</td>
            <td class="multiplier-td-content"><input type="text" class="fddb-multiplier" name="fddb-multiplier" id="fddb-multiplier"></td>
        </tr>
        <tr class="multiplier-table-row">
            <td class="multiplier-td-label">Net weight:</td>
            <td class="multiplier-td-content">
                <input type="text" class="fddb-multiplier net-weight-part" name="fddb-multiplier-net-weight-part" id="fddb-multiplier-net-weight-part">
                of
                <input type="text" value="" class="fddb-multiplier net-weight-whole" name="fddb-multiplier-net-weight-whole" id="fddb-multiplier-net-weight-whole">
            </td>
        </tr>
        <tr class="multiplier-table-row">
            <td class="multiplier-td-label">When:</td>
            <td class="multiplier-td-content">
                <input type="text" class="fddb-multiplier fddb-time-offset" value="0" name="fddb-time-offset" id="fddb-time-offset"> hrs. ago</td>
        </tr>
        <tr class="multiplier-table-row">
            <td class="multiplier-td-label"></td>
            <td class="multiplier-td-content"><input type="submit" value="Submit to fddb"></td>
        </tr>
    </table>
</form>
                </ul>
                <ul>
                    <li>
                        <b class="amount">1 </b>
                        <span>Zwiebel</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
                <ul>
                    <li>
                        <b class="amount">3 </b>
                        <span>Knoblauchzehen</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
                <ul>
                    <li>
                        <b class="amount">3 EL</b>
                        <span>Olivenöl</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
                <ul>
                    <li>
                        <b class="amount">240 g</b>
                        <span>Hokkaidokürbis</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
                <ul>
                    <li>
                        <b class="amount">1 1/2 TL</b>
                        <span>Kreuzkümmel</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
                <ul>
                    <li>
                        <b class="amount">1 TL</b>
                        <span>Koriander</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
                <ul>
                    <li>
                        <b class="amount">1/2 TL</b>
                        <span>Zimt</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
                <ul>
                    <li>
                        <b class="amount">1/2 TL</b>
                        <span>Kurkuma</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
                <ul>
                    <li>
                        <b class="amount">2 TL</b>
                        <span>Salz</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
                <ul>
                    <li>
                        <b class="amount">3 EL</b>
                        <span>Rosinen</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
                <ul>
                    <li>
                        <b class="amount">2 EL</b>
                        <span>Mandeln</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
                <ul>
                    <li>
                        <b class="amount">20 g</b>
                        <span>Petersilie</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
                <ul>
                    <li>
                        <b class="amount">15 g</b>
                        <span>Dill</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
                <ul>
                    <li>
                        <b class="amount">2 Zweige</b>
                        <span>Minze</span>
                        <i class="qualifier"></i>
                    </li>
                </ul>
            </nav>
            <article class="instructions-panel">
                <i class="img-toggle-button fa fa-camera fa-2x"></i>
                <div class="img-div">
                    <img class="textWrap" src="https://www.eat-this.org/wp-content/uploads/2019/09/eat-this-veganer-pilaw-one-pot-reispfanne_mit_kuerbis-2-1280x854.jpg"/>
                </div>
                <p class="instructions-para">Mandeln in einer heißen Pfanne ohne Öl 3 Minuten rösten und anschließend grob hacken.<br/>Petersilie, Dill, Minze zusammen fein hacken.<br/>Zwiebel und Knoblauch fein hacken. Öl in einen heißen Topf geben, Zwiebel- und Knoblauchwürfel bei mittlerer Hitze 3 Minuten unter Rühren anschwitzen.<br/>Reis hinzugeben und 5 Minuten bei mittlerer bis niedriger Hitze goldgelb braten. Ab und zu umrühren.<br/>Kürbis in 2 cm große Würfel schneiden und zusammen mit den Gewürzen in den Topf geben. Zwei Minuten schmoren lassen.<br/>Salz und Sultaninen hinzufügen und mit 500 ml Wasser ablöschen.<br/>Kurz aufkochen lassen und mit geschlossenem Deckel bei niedriger Hitze 30 Minuten köcheln lassen. <br/>Nicht mehr umrühren!<br/>Vor dem Servieren Reispfanne mit Kräutern und gehackten Mandeln toppen.</p>
            </article>
        </section>
        <footer>
            <b>Pro Portion:</b> <span class="nutritionValue">Kalorien: 217,74 kcal</span><span class="nutritionValue">Kohlenhydrate: 15,82 g</span><span class="nutritionValue">Protein: 3,42 g</span><span class="nutritionValue">Fett: 14,72 g</span><span class="nutritionValue">Ballaststoffe: 3,17 g</span><span class="nutritionValue">Zucker: 10,47 g</span><span class="nutritionValue">Kalzium: 44,67 mg</span><span class="nutritionValue">Eisen: 0,60 mg</span>
        </footer>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.img-toggle-button').click(function() {
                    $('.img-div').toggle();
                    $('.img-toggle-button').toggle();
                });
                $('.img-div').click(function() {
                    $('.img-div').toggle();
                    $('.img-toggle-button').toggle();
                });
                $('#post-to-fddb-button').click(function() {
                    $('.multiplier-panel').toggle();
                });
                $('form.multiplier-form').on('submit', function(e) {
                    e.preventDefault();
                    dlgMultiplier = calculateMultiplier();
                    dlgTimeOffset = calculateTimeOffset();
                    $.ajax({
                        type: "GET",
                        url: "/postToFddb.html?recipeId=" + recipeId + "&multiplier=" + dlgMultiplier + "&timeOffset=" + dlgTimeOffset,
                        data: "",
                        success: function(response) {
                            var x = 0;
                            window.location.href = "/listRecipes.html";
                        },
                        failure: function(errMsg) {
                            alert(errMsg);
                        }
                    });
                });
            });
        </script>
    </body>
</html>
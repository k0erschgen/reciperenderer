#!/bin/bash

APP_NAME="cookbook"
INSTALL_DIR="${COOKBOOK_HOST_HOME}/install"
TARGET_DEPLOY_DIR="${COOKBOOK_HOST_HOME}/${APP_NAME}"
TARGET_SSH="${COOKBOOK_USER}@${COOKBOOK_HOST}"

ssh ${TARGET_SSH} rm -rf ${INSTALL_DIR}
ssh ${TARGET_SSH} mkdir -p ${INSTALL_DIR}/${APP_NAME}
ssh ${TARGET_SSH} mkdir -p ${TARGET_DEPLOY_DIR}
scp build/distributions/${APP_NAME}*.zip ${TARGET_SSH}:${INSTALL_DIR}/${APP_NAME}/${APP_NAME}.zip

MY_PID=$(ssh ${TARGET_SSH} /usr/bin/ps -ef | grep ${APP_NAME} | grep -v 'grep' | cut -d' ' -f 5)
ssh ${TARGET_SSH} /bin/kill ${MY_PID}

ssh ${TARGET_SSH} rm -rf ${TARGET_DEPLOY_DIR}
ssh ${TARGET_SSH} mkdir -p ${TARGET_DEPLOY_DIR}
ssh ${TARGET_SSH} unzip ${INSTALL_DIR}/${APP_NAME}/${APP_NAME}.zip -d ${TARGET_DEPLOY_DIR}
ssh ${TARGET_SSH} mv -t ${TARGET_DEPLOY_DIR} ${TARGET_DEPLOY_DIR}/${APP_NAME}*/*

ssh ${TARGET_SSH} nohup ${TARGET_DEPLOY_DIR}/bin/${APP_NAME}
